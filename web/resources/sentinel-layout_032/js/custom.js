function skinChart() {
                this.cfg.shadow = false;
                this.cfg.seriesColors = ['#dae8ef','#009999'];
                this.cfg.grid = {
                    background: '#f3f5f7',
                    borderColor: '#e5ebf0',
                    gridLineColor: '#e5ebf0',
                    shadow: false
                };
                this.cfg.axesDefaults = {
                    rendererOptions: {
                       textColor: '#9fadb5' 
                    }
                };
                this.cfg.seriesDefaults = {
                    shadow: false,
                    markerOptions: {
                        shadow: false
                    }
                }
            }
            
                   function skinPie() {
                this.cfg.seriesColors = ['#dae8ef','#009999','#33ccff','#9fadb5'];
                this.cfg.grid = {
                    background: '#ffffff',
                    borderColor: '#eaeaea',
                    gridLineColor: '#e5ebf0',
                    shadow: false,
                    borderWidth: 0
                };
                this.cfg.seriesDefaults.shadow = false;
            }