/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author oluwasegun.idowu
 */
public class MainFilter implements Filter{
    
       private FilterConfig filterConfig;
    private final String LOGIN_PAGE = AppConstants.LOGIN_PAGE;
            private static final Logger logger = Logger.getLogger(MainFilter.class.getName());

    @Override
    public void init(FilterConfig fc) throws ServletException {
       this.filterConfig = fc; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fc) throws IOException, ServletException {
       
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        httpRequest.setCharacterEncoding("UTF-8");
        //logger.log(Level.INFO, "request uri... {0}", httpRequest.getRequestURI());
        //logger.log(Level.INFO, "context path... {0}", httpRequest.getContextPath());
        
         boolean loginPage = httpRequest.getRequestURI().equals(httpRequest.getContextPath() + LOGIN_PAGE);
           //      boolean dashboardPage = httpRequest.getRequestURI().equals(httpRequest.getContextPath() + AppConstants.DASHBOARD_PAGE);
         //logger.log(Level.INFO, "loginPage?... {0}", loginPage);
         if (loginPage
                || httpRequest.getServletPath().contains("/javax.faces.resource")) {
            fc.doFilter(request, response);
        } else {
            User user = null;
            if(session != null ) {
              user = (User) session.getAttribute(AppConstants.LOGIN_USER);  
            }
            if (/**!dashboardPage && **/(user == null || filterConfig == null)) {
                httpResponse.sendRedirect(httpRequest.getContextPath() + LOGIN_PAGE);
            } else {
                fc.doFilter(request, response);
            }
        } 
    }

    @Override
    public void destroy() {
       this.filterConfig = null; //To change body of generated methods, choose Tools | Templates.
    }
    
}
