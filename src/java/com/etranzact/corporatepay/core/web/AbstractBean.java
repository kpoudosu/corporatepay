/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.setup.web.UserSetupBean;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 * @param <T>
 */
public abstract class AbstractBean<T extends Serializable> implements Serializable {

    private Class<T> clazz;
    protected Logger log;
    protected T selectedItem;

    public AbstractBean(Class<T> classT) {
        this.clazz = classT;
        log = Logger.getLogger(clazz.getName());
    }

    protected abstract AbstractFacadeLocal<T> getFacade();

    public void onRowSelect(SelectEvent selectEvent) {
        log.log(Level.INFO, "selectEvent::: {0}", selectEvent);
        log.log(Level.INFO, "event::: {0}", selectEvent.getObject());
        setSelectedItem((T) selectEvent.getObject());
    }

    /**
     * @return the selectedItem
     */
    public T getSelectedItem() {

        try {
            if(selectedItem==null)  {
            this.selectedItem = clazz.newInstance();
            }
            return selectedItem;
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(AbstractBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * @param selectedItem the selectedItem to set
     */
    public void setSelectedItem(T selectedItem) {
        this.selectedItem = selectedItem;
    }

}
