/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.ocpsoft.shade.org.apache.commons.beanutils.BeanUtils;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author oluwasegun.idowu
 * @param <T>
 */
public abstract class AbstractTableBean<T extends Serializable> implements Serializable {

    private LazyDataModel<T> lazyDataModel;

    protected abstract AbstractFacadeLocal<T> getFacade();

    protected abstract TableProperties getTableProperties();
    private int rowcount = 20;

    protected Logger log;

    private Class<T> clazz;

    public AbstractTableBean(Class<T> classT) {
        this.clazz = classT;
        log = Logger.getLogger(clazz.getName());
    }

    @PostConstruct
    public void init() {
        lazyDataModel = new LazyDataModel<T>() {

            @Override
            public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    filters.putAll(getTableProperties().getAdditionalFilter());
                    try {
                        clazz.getDeclaredField("central");
                        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
                        if (!(loggedInUser instanceof BankUser || loggedInUser instanceof CorporateUser)) {
                            filters.put("central", Boolean.TRUE);
                        }
                    } catch (NoSuchFieldException nex) {

                    }
                    List<T> pageList = getFacade().findAll(first, pageSize, filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), new String[0]);
                    List<T> allList = getFacade().findAll(filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), new String[0]);
//                    log.log(Level.INFO, "pageList:: {0}", pageList.size());
//                    log.log(Level.INFO, "all:: {0}", allList.size());
                    lazyDataModel.setRowCount(allList.size());
                    if (sortField != null) {
                        Collections.sort(pageList, new LazySorter(sortField, sortOrder));
                    }
                    return pageList;
                } catch (Exception ex) {
                    Logger.getLogger(AbstractTableBean.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList<>();
                }
            }


            @Override
            public T getRowData(String rowKey) {
                return getTableRowData(rowKey);
            }

            @Override
            public Object getRowKey(T t) {
                return getTableKey(t);
            }
        };
    }



    public abstract T getTableRowData(String rowKey);

    public abstract Object getTableKey(T t);

    /**
     * @return the lazyDataModel
     */
    public LazyDataModel<T> getLazyDataModel() {
        return lazyDataModel;
    }

    /**
     * @param lazyDataModel the lazyDataModel to set
     */
    public void setLazyDataModel(LazyDataModel<T> lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }

    /**
     * @return the rowcount
     */
    public int getRowcount() {
        return rowcount;
    }

    /**
     * @param rowcount the rowcount to set
     */
    public void setRowcount(int rowcount) {
        this.rowcount = rowcount;
    }

    protected static class TableProperties {

        private boolean activeRecordsOnly;
        private String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED};
        private Map<String, Object> additionalFilter = new HashMap<>();

        public TableProperties() {
        }

        /**
         * @return the activeRecordsOnly
         */
        public boolean isActiveRecordsOnly() {
            return activeRecordsOnly;
        }

        /**
         * @param activeRecordsOnly the activeRecordsOnly to set
         */
        public void setActiveRecordsOnly(boolean activeRecordsOnly) {
            this.activeRecordsOnly = activeRecordsOnly;
        }

        /**
         * @return the flagStatuses
         */
        public String[] getFlagStatuses() {
            return flagStatuses;
        }

        /**
         * @param flagStatuses the flagStatuses to set
         */
        public void setFlagStatuses(String[] flagStatuses) {
            this.flagStatuses = flagStatuses;
        }

        /**
         * @return the additionalFilter
         */
        public Map<String, Object> getAdditionalFilter() {
            return additionalFilter;
        }

        /**
         * @param additionalFilter the additionalFilter to set
         */
        public void setAdditionalFilter(Map<String, Object> additionalFilter) {
            this.additionalFilter = additionalFilter;
        }

    }

}
