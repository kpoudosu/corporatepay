/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.UserFacadeLocal;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Menu;
import com.etranzact.corporatepay.model.MenuItem;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean extends AbstractBean<User> {

    private User loggedInUser;

    public UserBean() {
        super(User.class);
    }

    @EJB
    private UserFacadeLocal userFacadeLocal;
    private User user;
    private List<Menu> userMenus;

    @Override
    protected UserFacadeLocal getFacade() {
        return userFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public String login() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("..... login started....");
            setLoggedInUser(getFacade().login(user.getUsername(), user.getPassword()));
            log.log(Level.INFO, "..... login user ....{0}", getLoggedInUser());
            log.log(Level.INFO, "..... bankuser? ....{0}", getLoggedInUser() instanceof BankUser);
            log.log(Level.INFO, "..... corporateuser? ....{0}", getLoggedInUser() instanceof CorporateUser);
            if (getLoggedInUser() instanceof BankUser) {
                getLoggedInUser().setBankCode(((BankUser) getLoggedInUser()).getBank().getId());
            } else if (getLoggedInUser() instanceof CorporateUser) {
                getLoggedInUser().setBankCode(((CorporateUser) getLoggedInUser()).getCorporate().getBank().getId());
                if(!((CorporateUser) getLoggedInUser()).getCorporate().getActive()) {
                    throw new Exception("Corporate is inactive");
                }
                   if(!((CorporateUser) getLoggedInUser()).getCorporate().getFlagStatus().equals(AppConstants.APPROVED)) {
                    throw new Exception("Corporate has not being approved");
                }
            }
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.getSessionMap().put(AppConstants.LOGIN_USER, getLoggedInUser());
            return "/dashboard?faces-redirect=true";
        } catch (Exception ex) {
            log.log(Level.SEVERE, ".. error loggin in ...{0}", ex.getMessage());
            log.log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
            return null;
        }
    }

    public String logout() {
        log.info("..... logging out... ");
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ((HttpSession) externalContext.getSession(false)).invalidate();
        log.info("..... logged out user... ");
        return "/login?faces-redirect=true";
    }

    public void navigate() {

    }

    /**
     * @return the user
     */
    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the loggedInUser
     */
    public User getLoggedInUser() {
        return loggedInUser;
    }

    /**
     * @param loggedInUser the loggedInUser to set
     */
    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    /**
     * @return the userMenus
     */
    public List<Menu> getUserMenus() {
        Set<MenuItem> resources = loggedInUser.getRole().getResources();
        userMenus = new ArrayList<>();
        for (MenuItem menuItem : resources) {
            Menu menu = menuItem.getMenu();
            if (!menu.getItems().contains(menuItem)) {
                menu.getItems().add(menuItem);
            }
            if (!userMenus.contains(menu)) {
                userMenus.add(menu);
            }
        }
        Comparator<Menu> menuComparable = new Comparator<Menu>() {

            @Override
            public int compare(Menu o1, Menu o2) {
                return o1.getPosition() - o2.getPosition();
            }
        };
          Comparator<MenuItem> menuItemComparable = new Comparator<MenuItem>() {

            @Override
            public int compare(MenuItem o1, MenuItem o2) {
                return o1.getPosition() - o2.getPosition();
            }
        };
        Collections.sort(userMenus, menuComparable);
        for(Menu menu: userMenus) {
             Collections.sort(menu.getItems(), menuItemComparable); 
        }
        return userMenus;
    }

    /**
     * @param userMenus the userMenus to set
     */
    public void setUserMenus(List<Menu> userMenus) {
        this.userMenus = userMenus;
    }

}
