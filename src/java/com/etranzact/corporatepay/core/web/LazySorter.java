/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import java.io.Serializable;
    import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Oluwasegun.Idowu
 */

public class LazySorter<T extends Serializable> implements Comparator {
 
    private String sortField;
     
    private SortOrder sortOrder;
     
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    @Override
    public int compare(Object t1, Object t2) {
        try {
            Object value1 = t1.getClass().getField(this.sortField).get(t1);
            Object value2 = t2.getClass().getField(this.sortField).get(t2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
