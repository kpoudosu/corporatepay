/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.User;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Oluwasegun.Idowu
 * @param <T>
 */
public interface AbstractFacadeLocal<T extends Serializable> {

    T create(T entity) throws Exception;
    
    T find(Long id) throws Exception;
    
    List<T> findAll() throws Exception;
    
    List<T> findAll(int min, int max) throws Exception;
    
    List<T> findAll(Map<String, Object> filters, String... orderFields) throws Exception;
    
    List<T> findAll(int min, int max, Map<String, Object> filters, String... orderFields) throws Exception;
    
    List<T> findAll(Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception;
    
    List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception;
    
    List<T> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception;
    
    List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception;
    
    List<T> findAll(Map<String, Object> filters, boolean activeOnly) throws Exception;
    
    List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly) throws Exception;
    
     void setEntityNamePrefix(String prefix);
     

    


}
