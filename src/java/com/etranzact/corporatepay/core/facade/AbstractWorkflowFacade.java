/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankApprovalRoute;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.PersistenceException;

/**
 *
 * @author Oluwasegun.Idowu
 */
public abstract class AbstractWorkflowFacade extends AbstractFacade<ApprovalTask> {

    public AbstractWorkflowFacade() {
        super(ApprovalTask.class);
    }

    public ApprovalTask startWorkflow(Class clazz, String objectId, byte[] attachment) throws Exception {
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();
        } catch (PersistenceException pex) {
            throw new PersistenceException("error starting approval route");
        }
        if (approvalType != null) {
            ApprovalRoute approvalRoute;
            try {
                approvalRoute = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level=1 and a.active=true and a.flagStatus=:flagStatus", ApprovalRoute.class)
                        .setParameter("approvalType", approvalType).setParameter("flagStatus", AppConstants.APPROVED).getSingleResult();
            } catch (PersistenceException pex) {
                throw new PersistenceException("error starting approval route");
            }
            if (approvalRoute != null && approvalRoute.getActive() && approvalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(approvalRoute);
                approvalTask.setTargetObjectId(objectId);
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject());
                approvalTask.setTaskAccessor(approvalRoute.getApprover());
                approvalTask.setDocument(attachment);
                return super.create(approvalTask);
            }
        }
        return null;
    }

    public ApprovalTask startWorkflow(Class clazz, String objectId, byte[] attachment, Corporate corporate) throws Exception {
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();
        } catch (PersistenceException pex) {
            throw new PersistenceException("error starting approval route");
        }
        if (approvalType != null) {
            CorporateApprovalRoute approvalRoute;
            try {
                approvalRoute = getEntityManager().createQuery("select a from CorpApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level=1 and a.active=true and a.flagStatus=:flagStatus and a.corporate=:corporate", CorporateApprovalRoute.class)
                        .setParameter("approvalType", approvalType).setParameter("flagStatus", AppConstants.APPROVED).setParameter("corporate", corporate).getSingleResult();
            } catch (PersistenceException pex) {
                throw new PersistenceException("error starting approval route");
            }
            if (approvalRoute != null && approvalRoute.getActive() && approvalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(approvalRoute);
                approvalTask.setTargetObjectId(objectId);
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject());
                approvalTask.setTaskAccessor(approvalRoute.getApprover());
                approvalTask.setDocument(attachment);
                return super.create(approvalTask);
            }
        }
        return null;
    }

    public ApprovalTask startWorkflow(Class clazz, String objectId, byte[] attachment, Bank bank) throws Exception {
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();
        } catch (PersistenceException pex) {
            throw new PersistenceException("error starting approval route");
        }
        if (approvalType != null) {
            BankApprovalRoute approvalRoute;
            try {
                approvalRoute = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level=1 and a.active=true and a.flagStatus=:flagStatus and a.bank=:bank", BankApprovalRoute.class)
                        .setParameter("approvalType", approvalType).setParameter("flagStatus", AppConstants.APPROVED).setParameter("bank", bank).getSingleResult();
            } catch (PersistenceException pex) {
                throw new PersistenceException("error starting approval route");
            }
            if (approvalRoute != null && approvalRoute.getActive() && approvalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(approvalRoute);
                approvalTask.setTargetObjectId(objectId);
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject());
                approvalTask.setTaskAccessor(approvalRoute.getApprover());
                approvalTask.setDocument(attachment);
                return super.create(approvalTask);
            }
        }
        return null;
    }

    public ApprovalTask doTaskAction(ApprovalTask approvalTask, boolean approved, boolean noAction) throws Exception {
        String actionFlag = noAction ? AppConstants.KEPT_IN_VIEW : approved ? AppConstants.APPROVED : AppConstants.REJECTED;
        approvalTask.setApprovalAction(actionFlag);
        approvalTask.setActionDate(new Date());
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        approvalTask.setActor((Approver) externalContext.getSessionMap().get(AppConstants.LOGIN_USER));
        ApprovalTask mergerdApprovalTask = super.create(approvalTask);
        ApprovalRoute approvalRoute = mergerdApprovalTask.getApprovalRoute();
        ApprovalTask nextApprovalTask = null;
        ApprovalRoute nextApprovalRoute;
        if (approvalRoute instanceof BankApprovalRoute) {
            nextApprovalRoute = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                    + " and a.level=" + approvalRoute.getLevel() + 1 + " and a.active=true and a.flagStatus=:flagStatus and a.bank=:bank", BankApprovalRoute.class)
                    .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).setParameter("bank", ((BankApprovalRoute) approvalRoute).getBank()).getSingleResult();
        } else if (approvalRoute instanceof CorporateApprovalRoute) {
            nextApprovalRoute = getEntityManager().createQuery("select a from CorpApprovalRoute a where a.approvalType=:approvalType"
                    + " and a.level=" + approvalRoute.getLevel() + 1 + " and a.active=true and a.flagStatus=:flagStatus and a.corporate=:corporate", CorporateApprovalRoute.class)
                    .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).setParameter("corporate", ((CorporateApprovalRoute) approvalRoute).getCorporate()).getSingleResult();
        } else {
            nextApprovalRoute = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType"
                    + " and a.level=" + approvalRoute.getLevel() + 1 + " and a.active=true and a.flagStatus=:flagStatus", ApprovalRoute.class)
                    .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).getSingleResult();
        }
        if (nextApprovalRoute != null && nextApprovalRoute.getActive() && nextApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
            nextApprovalTask = new ApprovalTask();
            nextApprovalTask.setApprovalRoute(nextApprovalRoute);
            nextApprovalTask.setTargetObjectId(mergerdApprovalTask.getTargetObjectId());
            approvalTask.setDateCreated(new Date());
            approvalTask.setNameValuePairs(getTaskObject());
            approvalTask.setTaskAccessor(nextApprovalRoute.getApprover());
            approvalTask.setDocument(mergerdApprovalTask.getDocument());
            return super.create(approvalTask);
        } else {
            completeWorkflow(mergerdApprovalTask);
        }

        return nextApprovalTask;
    }

    public List<ApprovalTask> findUserApprovalTasks() throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User user = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        List<ApprovalTask> userTaskList = getEntityManager().createQuery("select a from ApprovalTask a where a.taskAccessor=:taskAccessor and a.approvalAction != null", ApprovalTask.class).setParameter("taskAccessor", user).getResultList();
        for (ApprovalTask approvalTask : userTaskList) {
            approvalTask.setTaskType(AppConstants.USER_TASK_TYPE);
        }
        for (ApprovalGroup approvalGroup : user.getApprovalGroups()) {
            List<ApprovalTask> groupTaskList = getEntityManager().createQuery("select a from ApprovalTask a where a.taskAccessor=:taskAccessor and a.approvalAction != null", ApprovalTask.class).setParameter("taskAccessor", approvalGroup).getResultList();
            userTaskList.addAll(groupTaskList);
        }
        return userTaskList;
    }

    public abstract Set<TaskObjectNameValuePair> getTaskObject();

    public abstract ApprovalTask completeWorkflow(ApprovalTask currApprovalTask);
}
