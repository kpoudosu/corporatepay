/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.Approver;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Oluwasegun.Idowu
 * @param <T>
 */
public abstract class AbstractFacade<T extends Serializable> implements AbstractFacadeLocal<T>, Serializable {

    private Class<T> t;

    protected abstract EntityManager getEntityManager();
    protected Logger log;
    protected String entityNamePrefix = "";

    public AbstractFacade(Class<T> classT) {
        this.t = classT;
        log = Logger.getLogger(t.getName());
    }

    @Override
    public T create(T entity) throws Exception {
        return getEntityManager().merge(entity);
    }

    protected abstract T edit(T entity) throws Exception;

    @Override
    public T find(Long id) throws Exception {
        return getEntityManager().find(t, id);
    }

    @Override
    public List<T> findAll() throws Exception {
        String c = entityIdentifier();
        return getEntityManager().createQuery("select " + c + " from " + t.getSimpleName() + " " + c).getResultList();
    }

    @Override
    public List<T> findAll(int min, int max) throws Exception {
        String c = entityIdentifier();
        return getEntityManager().createQuery("select " + c + " from " + t.getSimpleName() + " " + c).setFirstResult(min).setMaxResults(max).getResultList();
    }

    private Query getQuery(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        String c = entityIdentifier();
        String orderClause = "";
        if (orderFields.length > 0) {
            int i = 0;
            orderClause += " order by " + c + ".";
            for (String orderField : orderFields) {
                i++;
                if (orderField == null) {
                    orderClause = "";
                }
                orderClause += i == orderFields.length ? orderField : orderField + ",";
            }
        }
        String filterWhere = "";
        if (filters != null) {
            int i = 0;
            filterWhere += filters.isEmpty() ? "" : " where ";
            for (Map.Entry<String, Object> entry : filters.entrySet()) {
                i++;
                String f;
                if (entry.getValue() != null) {
                    f = c + "." + entry.getKey() + "=:" + entry.getKey();
                } else {
                    f = entry.getKey();
                }
                filterWhere += i == filters.size() ? f : f + " and ";
            }
        }
        filterWhere += activeOnly ? filterWhere.contains("where") ? (" and " + c + ".active=" + Boolean.TRUE) : (" where " + c + ".active=" + Boolean.TRUE) : "";

        String flagStatusClause = "";

        if (flagStatuses != null && flagStatuses.length > 1) {
            int i = 0;
            flagStatusClause += filterWhere.contains("where") ? " and " : " where ";
            flagStatusClause += c + ".flagStatus in ('";
            for (String flagStatus : flagStatuses) {
                i++;
                flagStatusClause += i == flagStatuses.length ? flagStatus + "')" : flagStatus + "','";
            }
        }

        log.log(Level.INFO, "filterWhere: {0}", filterWhere);
        log.log(Level.INFO, "flagStatusClause: {0}", flagStatusClause);
        log.log(Level.INFO, "orderClause: {0}", orderClause);

        String query = "select " + c + " from " + entityNamePrefix + t.getSimpleName() + " " + c + filterWhere + flagStatusClause + (orderClause.equals("null") ? "" : orderClause);
        return getEntityManager().createQuery(query);
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, false, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, false, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.getResultList();
    }

    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, flagStatuses, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, flagStatuses, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    protected abstract T approve(T entity) throws Exception;

    protected abstract T reject(T entity) throws Exception;

    private String entityIdentifier() {
        return String.valueOf(t.getSimpleName().charAt(0)).toLowerCase();
    }

    @Override
    public void setEntityNamePrefix(String prefix) {
        entityNamePrefix = prefix;
    }
    


}
