/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public UserFacade() {
        super(User.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User login(String username, String password) throws Exception {
        List<User> users = em.createQuery("select u from User u where u.username=:username and u.password=:password", User.class)
                .setParameter("username", username).setParameter("password", password).getResultList();
        if (users.isEmpty()) {
            throw new Exception("user not found");
        } else if (users.size() > 1) {
            throw new Exception("unexpected no of users found");
        } else {
            User user = users.get(0);
            String validateUserMsg = validateUser(user);
            if (validateUserMsg != null) {
                throw new Exception(validateUserMsg);
            }
            return user;
        }
    }

    @Override
    protected User edit(User entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User approve(User entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User reject(User entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String validateUser(User user) {
        String customeExMsg = null;
        Calendar calendar = Calendar.getInstance();
        if (user.getFlagStatus().equals(AppConstants.REJECTED) || user.getFlagStatus().equals(AppConstants.CREATED)) {
            customeExMsg = "user has not being approved";
        }
        if (!user.getActive()) {
            customeExMsg = "user not active";
        }
        if (user.getChangePassword()) {
            customeExMsg = "user password has expired, must be changed";
        }
        if (user.getUserDisabled()) {
            customeExMsg = "user has being disabled";
        }
        if (user.getUserLocked()) {
            customeExMsg = "user's account has been locked";
        }
        if (user.getPasswordDateExpired() != null) {
            if (user.getPasswordDateExpired().getTime() < calendar.getTimeInMillis()) {
                customeExMsg = "user's password has expired";
            }
        }
        if (user.getFromTimeAccess() != null || user.getToTimeAccess() != null) {
            boolean accessTimeFailed = false;
            if (user.getToTimeAccess() == null) {
                if (user.getFromTimeAccess().getTime() > calendar.getTimeInMillis()) {
                    accessTimeFailed = true;
                }
            } else if (user.getFromTimeAccess() == null) {
                if (user.getToTimeAccess().getTime() < calendar.getTimeInMillis()) {
                    accessTimeFailed = true;
                }
            } else {
                log.log(Level.INFO, "from {0}", user.getFromTimeAccess().getTime());
                log.log(Level.INFO, "to {0}", user.getToTimeAccess().getTime());
                log.log(Level.INFO, " {0}", calendar.getTimeInMillis());
                if ((user.getFromTimeAccess().getTime() > calendar.getTimeInMillis())
                        || (user.getToTimeAccess().getTime() < calendar.getTimeInMillis())) {
                    accessTimeFailed = true;
                }
            }
            customeExMsg = accessTimeFailed ? "user has no access to Corporate Pay at this time" : customeExMsg;
        }

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        boolean dayFailed = dayOfWeek == 1 ? user.getSunday().equals(AppConstants.TURNED_OFF)
                : dayOfWeek == 2 ? user.getMonday().equals(AppConstants.TURNED_OFF)
                        : dayOfWeek == 3 ? user.getTuesday().equals(AppConstants.TURNED_OFF)
                                : dayOfWeek == 4 ? user.getWednesday().equals(AppConstants.TURNED_OFF)
                                        : dayOfWeek == 5 ? user.getThursday().equals(AppConstants.TURNED_OFF)
                                                : dayOfWeek == 6 ? user.getFriday().equals(AppConstants.TURNED_OFF)
                                                        : dayOfWeek == 7 ? user.getSunday().equals(AppConstants.TURNED_OFF) : false;
        customeExMsg = dayFailed ? "user has no access to Corporate Pay on " + calendar.getDisplayNames(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH) : customeExMsg;

        return customeExMsg;
    }

}
