/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class ApprovalRouteFacade extends AbstractFacade<ApprovalRoute> implements ApprovalRouteFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public ApprovalRouteFacade() {
        super(ApprovalRoute.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ApprovalRoute edit(ApprovalRoute entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ApprovalRoute approve(ApprovalRoute entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ApprovalRoute reject(ApprovalRoute entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

        @Override
    public boolean approvalRouteExist(Class clazz) throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        Map<String,Object> filter = new HashMap<>();
        if(loggedInUser instanceof BankUser) {
            BankUser buser = (BankUser) loggedInUser;
            filter.put("bank", buser.getBank());
            entityNamePrefix = "Bank";
        } else if(loggedInUser instanceof CorporateUser){
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            filter.put("corporate", corporateUser.getCorporate());
              entityNamePrefix = "Corporate";
        } else {
               filter.put("central", Boolean.TRUE); 
        }
        String q = "select a from ApprovalType a where a.description='" + clazz.getSimpleName() + "'";
        log.info(q);
        ApprovalType approvalType = getEntityManager().createQuery(q,ApprovalType.class).getSingleResult();
        filter.put("approvalType", approvalType);
        String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED};
        List<ApprovalRoute> findAll = findAll(filter, true,flagStatuses,new String[0]);
        return !findAll.isEmpty();
    }
    
  
   
}
