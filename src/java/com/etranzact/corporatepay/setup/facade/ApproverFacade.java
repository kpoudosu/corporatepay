/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankApprovalGroup;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateApprovalGroup;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.web.ApprovalRouteBean;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class ApproverFacade extends AbstractFacade<Approver> implements ApproverFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;

    public ApproverFacade() {
        super(Approver.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Approver edit(Approver entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Approver approve(Approver entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Approver reject(Approver entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Approver> findApprovers() throws Exception {
        List<Approver> approvers = new ArrayList<>();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (loggedInUser instanceof BankUser) {
            BankUser buser = (BankUser) loggedInUser;
            List<BankUser> bankUsers = getEntityManager().createQuery("select a from BankUser a where a.bank=:bank and a.flagStatus=:flagStatus", BankUser.class)
                    .setParameter("bank", buser.getBank()).setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            approvers.addAll(bankUsers);
            List<BankApprovalGroup> bankAppGroupUsers = getEntityManager().createQuery("select a from BankApprovalGroup a where a.bank=:bank and a.flagStatus=:flagStatus", BankApprovalGroup.class)
                    .setParameter("bank", buser.getBank()).setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            approvers.addAll(bankAppGroupUsers);
        } else if (loggedInUser instanceof CorporateUser) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            List<CorporateUser> corporateUsers = getEntityManager().createQuery("select a from CorporateUser a where a.corporate=:corporate and a.flagStatus=:flagStatus", CorporateUser.class)
                    .setParameter("corporate", corporateUser.getCorporate()).setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            approvers.addAll(corporateUsers);
            List<CorporateApprovalGroup> corporateAppGroupUsers = getEntityManager().createQuery("select a from CorporateApprovalGroup a where a.corporate=:corporate and a.flagStatus=:flagStatus", CorporateApprovalGroup.class)
                    .setParameter("corporate", corporateUser.getCorporate()).setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            approvers.addAll(corporateAppGroupUsers);
        } else {
            List<User> corporateUsers = getEntityManager().createQuery("select a from User a where a.flagStatus=:flagStatus and a.central=true", User.class)
                    .setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            approvers.addAll(corporateUsers);
            List<ApprovalGroup> appGroupUsers = getEntityManager().createQuery("select a from ApprovalGroup a where  a.flagStatus=:flagStatus and  a.central=true", ApprovalGroup.class)
                    .setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            approvers.addAll(appGroupUsers);
        }
        return approvers;
    }

    @Override
    public List<Approver> findApproversExcluding() throws Exception {
        List<Approver> approvers = new ArrayList<>();
        List<Approver> findApprovers = findApprovers();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        ApprovalRouteBean approvalRouteBean = (ApprovalRouteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("approvalRouteBean");
        ApprovalType approvalType = approvalRouteBean.getApprovalType();
        log.log(Level.INFO, "approvalType || approvalType || approvalType {0}", approvalType);
        Map<String, Object> map = new HashMap<>();
        if (loggedInUser instanceof BankUser) {
            BankUser bankUser = (BankUser) loggedInUser;
            map.put("bank", bankUser.getBank());
            approvalRouteFacadeLocal.setEntityNamePrefix("Bank");
        } else if (loggedInUser instanceof CorporateUser) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            map.put("corporate", corporateUser.getCorporate());
            approvalRouteFacadeLocal.setEntityNamePrefix("Corporate");
        }
        map.put("approvalType", approvalType);
        List<ApprovalRoute> approvalRoutes = approvalRouteFacadeLocal.findAll(map, true, new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED}, new String[0]);
        outer:
        for (Approver approver : findApprovers) {
            for (ApprovalRoute approvalRoute : approvalRoutes) {
                if (approvalRoute.getApprover().equals(approver)) {
                    continue outer;
                }
            }
            approvers.add(approver);
        }
        return approvers;
    }

}
