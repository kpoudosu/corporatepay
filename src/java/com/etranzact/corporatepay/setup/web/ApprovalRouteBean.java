/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankApprovalRoute;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApproverFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "approvalRouteBean")
@SessionScoped
public class ApprovalRouteBean extends AbstractBean<ApprovalRoute> {

    private boolean showAppRoute;

    public ApprovalRouteBean() {
        super(ApprovalRoute.class);
    }

    private ApprovalType approvalType;
    @EJB
    private ApproverFacadeLocal approverFacadeLocal;
    private List<Approver> approvers;

    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;

    @Override
    protected ApprovalRouteFacadeLocal getFacade() {
        return approvalRouteFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("savingg...........");
            log.log(Level.INFO, "saving for ...........{0}", selectedItem.getApprovalType());
            log.log(Level.INFO, "saving for ...........{0}", selectedItem.getApprover());
            approvalType = selectedItem.getApprovalType();
            selectedItem.setFlagStatus(AppConstants.APPROVED);
            approvalRouteFacadeLocal.create(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Approval Route Created");
            FacesContext.getCurrentInstance().addMessage(null, m);
            selectedItem = null;
            selectedItem = prepObject(selectedItem);
            selectedItem.setApprovalType(approvalType);
        } catch (Exception ex) {
            Logger.getLogger(ApprovalRouteBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        Object obj = selectEvent.getObject();
        showAppRoute = true;
        if (obj instanceof ApprovalType) {
            approvalType = (ApprovalType) selectEvent.getObject();
            try {
                approvers = approverFacadeLocal.findApproversExcluding();
            } catch (Exception ex) {
                Logger.getLogger(ApprovalRouteTableBean.class.getName()).log(Level.SEVERE, null, ex);
                approvers = new ArrayList<>();
            }
        } else {
            super.onRowSelect(selectEvent);
        }
        selectedItem.setApprovalType(approvalType);
    }

    @Override
    public ApprovalRoute getSelectedItem() {
        selectedItem = prepObject(selectedItem);
        return selectedItem;
    }

    /**
     * @return the approvalType
     */
    public ApprovalType getApprovalType() {
        return approvalType;
    }

    /**
     * @param approvalType the approvalType to set
     */
    public void setApprovalType(ApprovalType approvalType) {
        this.approvalType = approvalType;
    }

    /**
     * @return the approvers
     */
    public List<Approver> getApprovers() {
        return approvers;
    }

    /**
     * @param approvers the approvers to set
     */
    public void setApprovers(List<Approver> approvers) {
        this.approvers = approvers;
    }

    /**
     * @return the showAppRoute
     */
    public boolean isShowAppRoute() {
        return showAppRoute;
    }

    /**
     * @param showAppRoute the showAppRoute to set
     */
    public void setShowAppRoute(boolean showAppRoute) {
        this.showAppRoute = showAppRoute;
    }

    private ApprovalRoute prepObject(ApprovalRoute approvalRoute) {
        if (approvalRoute == null) {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            if (loggedInUser instanceof BankUser) {
                approvalRoute = new BankApprovalRoute();
                ((BankApprovalRoute) approvalRoute).setBank(((BankUser) loggedInUser).getBank());
            } else if (loggedInUser instanceof CorporateUser) {
                approvalRoute = new CorporateApprovalRoute();
                ((CorporateApprovalRoute) approvalRoute).setCorporate(((CorporateUser) loggedInUser).getCorporate());
            } else {
                approvalRoute = new ApprovalRoute();
            }
        }
        return approvalRoute;
    }

}
