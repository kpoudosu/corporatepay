/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "userSetupTableBean")
@SessionScoped
public class UserSetupTableBean extends AbstractTableBean<User> {

    @EJB
    private UserSetupFacadeLocal userFacadeLocal;

    public UserSetupTableBean() {
        super(User.class);
    }

    @Override
    protected UserSetupFacadeLocal getFacade() {
        return userFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties tableProperties = new TableProperties();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (loggedInUser instanceof BankUser) {
            BankUser bankUser = (BankUser) loggedInUser;
            tableProperties.getAdditionalFilter().put("bank", bankUser.getBank());
            userFacadeLocal.setEntityNamePrefix("Bank");
        } else if (loggedInUser instanceof CorporateUser) {
            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            tableProperties.getAdditionalFilter().put("corporate", corporateUser.getCorporate());
            userFacadeLocal.setEntityNamePrefix("Corporate");
        }
        return new TableProperties();
    }

    @Override
    public User getTableRowData(String rowKey) {
        List<User> wrappedData = (List<User>) getLazyDataModel().getWrappedData();
        for (User t : wrappedData) {
            if (Long.parseLong(rowKey) == t.getId()) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(User t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

}
