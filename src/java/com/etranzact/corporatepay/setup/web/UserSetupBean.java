/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "userSetupBean")
@RequestScoped
public class UserSetupBean extends AbstractBean<User> {

//    private User selectedUser;

    public UserSetupBean() {
        super(User.class);
    }

    @EJB
    private UserSetupFacadeLocal userFacadeLocal;
    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;

    @Override
    protected UserSetupFacadeLocal getFacade() {
        return userFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("savingg...........");
            boolean approvalRouteExist = approvalRouteFacadeLocal.approvalRouteExist(User.class);
            if (!approvalRouteExist) {
               throw new Exception("Cannot Save User: Approval Route does not exist");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }
    
//    @Override
//            public void onRowSelect(SelectEvent selectEvent) {
//        selectedItem = (User) selectEvent.getObject();
//    }

//    /**
//     * @return the selectedUser
//     */
//    public User getSelectedUser() {
//        if(selectedUser==null) {
//            selectedUser = new User();
//        }
//        return selectedUser;
//    }
//
//    /**
//     * @param selectedUser the selectedUser to set
//     */
//    public void setSelectedUser(User selectedUser) {
//        this.selectedUser = selectedUser;
//    }

    
}
