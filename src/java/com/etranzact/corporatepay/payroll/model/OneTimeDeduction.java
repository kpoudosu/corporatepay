/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_ONETIME_DEDUCTION")
@Inheritance(strategy = InheritanceType.JOINED)
@Audited
public class OneTimeDeduction extends OtherDeduction {


            @Column(name="AMOUNT_TYPE")
    private String amountType;
                @Column(name="TMP_AMOUNT_TYPE")
    private String tmpAmountType;
        @Column(name="AMOUNT")
     private BigDecimal amount =new BigDecimal("0.0");
                @Column(name="TMP_AMOUNT")
     private BigDecimal tmpAmount =new BigDecimal("0.0");
   

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

   

    /**
     * @return the tmpAmountType
     */
    public String getTmpAmountType() {
        return tmpAmountType;
    }

    /**
     * @param tmpAmountType the tmpAmountType to set
     */
    public void setTmpAmountType(String tmpAmountType) {
        this.tmpAmountType = tmpAmountType;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

      

}
