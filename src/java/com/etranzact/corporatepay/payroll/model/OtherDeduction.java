/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_OTHER_DEDUCTION", uniqueConstraints = {@UniqueConstraint(columnNames = {"DEDUCTION_NAME"})})
@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class OtherDeduction implements Serializable {

    @Id
      @GeneratedValue
    @Column(name = "DEDUCTION_ID")
    private Long Id;
    @Column(name="DEDUCTION_TYPE")
    private String deductionType;
    @Column(name="TMP_DEDUCTION_TYPE")
      private String tmpDeductionType;
       @Column(name="DEDUCTION_NAME")
    private String deductionName;
             @Column(name="TMP_DEDUCTION_NAME")
    private String tmpDeductionName;
                  @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
           @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
        @Column(name = "FIGURES_STATIC")
    private Boolean figuresStatic = Boolean.FALSE;
                @Column(name = "TMP_FIGURES_STATIC")
    private Boolean tmpFiguresStatic = Boolean.FALSE;


    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

   

    /**
     * @return the deductionName
     */
    public String getDeductionName() {
        return deductionName;
    }

    /**
     * @param deductionName the deductionName to set
     */
    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

  

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the deductionType
     */
    public String getDeductionType() {
        return deductionType;
    }

    /**
     * @param deductionType the deductionType to set
     */
    public void setDeductionType(String deductionType) {
        this.deductionType = deductionType;
    }

    /**
     * @return the tmpDeductionType
     */
    public String getTmpDeductionType() {
        return tmpDeductionType;
    }

    /**
     * @param tmpDeductionType the tmpDeductionType to set
     */
    public void setTmpDeductionType(String tmpDeductionType) {
        this.tmpDeductionType = tmpDeductionType;
    }

    /**
     * @return the figuresStatic
     */
    public Boolean getFiguresStatic() {
        return figuresStatic;
    }

    /**
     * @param figuresStatic the figuresStatic to set
     */
    public void setFiguresStatic(Boolean figuresStatic) {
        this.figuresStatic = figuresStatic;
    }

    /**
     * @return the tmpDeductionName
     */
    public String getTmpDeductionName() {
        return tmpDeductionName;
    }

    /**
     * @param tmpDeductionName the tmpDeductionName to set
     */
    public void setTmpDeductionName(String tmpDeductionName) {
        this.tmpDeductionName = tmpDeductionName;
    }

    /**
     * @return the tmpFiguresStatic
     */
    public Boolean getTmpFiguresStatic() {
        return tmpFiguresStatic;
    }

    /**
     * @param tmpFiguresStatic the tmpFiguresStatic to set
     */
    public void setTmpFiguresStatic(Boolean tmpFiguresStatic) {
        this.tmpFiguresStatic = tmpFiguresStatic;
    }
    
    

}
