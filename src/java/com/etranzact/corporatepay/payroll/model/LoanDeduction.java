/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_LOAN_DEDUCTION")
@Audited
public class LoanDeduction extends OtherDeduction{

         @Column(name="PRINCIPAL_AMOUNT")
     private BigDecimal amount =new BigDecimal("0.0");
                 @Column(name="TMP_PRINCIPAL_AMOUNT")
     private BigDecimal tmpAmount =new BigDecimal("0.0");
              @Column(name="DEDUCTION_AMOUNT")
     private BigDecimal deductionAmount =new BigDecimal("0.0");
                      @Column(name="TMP_DEDUCTION_AMOUNT")
     private BigDecimal tmpDeductionAmount =new BigDecimal("0.0");

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the deductionAmount
     */
    public BigDecimal getDeductionAmount() {
        return deductionAmount;
    }

    /**
     * @param deductionAmount the deductionAmount to set
     */
    public void setDeductionAmount(BigDecimal deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

    /**
     * @return the tmpDeductionAmount
     */
    public BigDecimal getTmpDeductionAmount() {
        return tmpDeductionAmount;
    }

    /**
     * @param tmpDeductionAmount the tmpDeductionAmount to set
     */
    public void setTmpDeductionAmount(BigDecimal tmpDeductionAmount) {
        this.tmpDeductionAmount = tmpDeductionAmount;
    }
              
              

}
