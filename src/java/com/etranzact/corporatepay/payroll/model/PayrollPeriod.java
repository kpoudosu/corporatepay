/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_PAYROLL_PERIOD")
@Audited
public class PayrollPeriod implements Serializable {

    @Id
    @Size(min = 3, max = 6)
    @Column(name = "PAYROLL_PERIOD_TAG")
    private String tag;
    @Column(name = "PAYROLL_PERIOD_DESC")
    private String payrollPeriodDesc;
    @Column(name = "TMP_PAYROLL_PERIOD_DESC")
    private String tmpPayrollPeriodDesc;
    @Column(name = "EFFECTIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    @Column(name = "TMP_EFFECTIVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date tmpeffectiveDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Column(name = "TMP_END_DATE")
    @Temporal(TemporalType.DATE)
    private Date tmpEndDate;
     @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
                  @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
          @Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(mappedBy = "payrollPeriod",cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<WorkerSalary> workerSalarys = new HashSet<>();
                   @Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_PR_TMP_PAYROLLPERIOD_SALARY", joinColumns =
    @JoinColumn(name = "PERIOD_ID"), inverseJoinColumns =
    @JoinColumn(name = "SALARY_ID"))
    private Set<WorkerSalary> tmpWorkerSalarys = new HashSet<>();
                   
                   

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return the payrollPeriodDesc
     */
    public String getPayrollPeriodDesc() {
        return payrollPeriodDesc;
    }

    /**
     * @param payrollPeriodDesc the payrollPeriodDesc to set
     */
    public void setPayrollPeriodDesc(String payrollPeriodDesc) {
        this.payrollPeriodDesc = payrollPeriodDesc;
    }

    /**
     * @return the tmpPayrollPeriodDesc
     */
    public String getTmpPayrollPeriodDesc() {
        return tmpPayrollPeriodDesc;
    }

    /**
     * @param tmpPayrollPeriodDesc the tmpPayrollPeriodDesc to set
     */
    public void setTmpPayrollPeriodDesc(String tmpPayrollPeriodDesc) {
        this.tmpPayrollPeriodDesc = tmpPayrollPeriodDesc;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the tmpeffectiveDate
     */
    public Date getTmpeffectiveDate() {
        return tmpeffectiveDate;
    }

    /**
     * @param tmpeffectiveDate the tmpeffectiveDate to set
     */
    public void setTmpeffectiveDate(Date tmpeffectiveDate) {
        this.tmpeffectiveDate = tmpeffectiveDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the tmpEndDate
     */
    public Date getTmpEndDate() {
        return tmpEndDate;
    }

    /**
     * @param tmpEndDate the tmpEndDate to set
     */
    public void setTmpEndDate(Date tmpEndDate) {
        this.tmpEndDate = tmpEndDate;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the workerSalarys
     */
    public Set<WorkerSalary> getWorkerSalarys() {
        return workerSalarys;
    }

    /**
     * @param workerSalarys the workerSalarys to set
     */
    public void setWorkerSalarys(Set<WorkerSalary> workerSalarys) {
        this.workerSalarys = workerSalarys;
    }

    /**
     * @return the tmpWorkerSalarys
     */
    public Set<WorkerSalary> getTmpWorkerSalarys() {
        return tmpWorkerSalarys;
    }

    /**
     * @param tmpWorkerSalarys the tmpWorkerSalarys to set
     */
    public void setTmpWorkerSalarys(Set<WorkerSalary> tmpWorkerSalarys) {
        this.tmpWorkerSalarys = tmpWorkerSalarys;
    }

    

}
