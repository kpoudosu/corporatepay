/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKER_SALARY")
@Audited
public class WorkerSalary implements Serializable {
    @ManyToOne
    private PayrollPeriod payrollPeriod;


    @Id
    @GeneratedValue
    @Column(name = "SALARY_ID")
    private Long Id;
    @ManyToOne
    @JoinColumn(name = "WORKER_ID")
    private Worker worker;
       @Column(name = "AMOUNT")
    private BigDecimal amount = new BigDecimal("0.0");
            @Audited(targetAuditMode = NOT_AUDITED)
    @OrderColumn
  @OneToMany(mappedBy = "workerSalary",cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<SalaryDeduction> salaryDeductions = new HashSet<>();
                @OrderColumn
    @JoinTable(name = "COP_PR_WORKERSSALARY_STATUTORYDEDUCTION", joinColumns =
    @JoinColumn(name = "SALARY_ID"), inverseJoinColumns =
    @JoinColumn(name = "STATUTORY_DEDUCTION_ID"))
  @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<StatutoryDeduction> statutoryDeductions = new HashSet<>();

    /**
     * @return the payrollPeriod
     */
    public PayrollPeriod getPayrollPeriod() {
        return payrollPeriod;
    }

    /**
     * @param payrollPeriod the payrollPeriod to set
     */
    public void setPayrollPeriod(PayrollPeriod payrollPeriod) {
        this.payrollPeriod = payrollPeriod;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the worker
     */
    public Worker getWorker() {
        return worker;
    }

    /**
     * @param worker the worker to set
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the salaryDeductions
     */
    public Set<SalaryDeduction> getSalaryDeductions() {
        return salaryDeductions;
    }

    /**
     * @param salaryDeductions the salaryDeductions to set
     */
    public void setSalaryDeductions(Set<SalaryDeduction> salaryDeductions) {
        this.salaryDeductions = salaryDeductions;
    }

    /**
     * @return the statutoryDeductions
     */
    public Set<StatutoryDeduction> getStatutoryDeductions() {
        return statutoryDeductions;
    }

    /**
     * @param statutoryDeductions the statutoryDeductions to set
     */
    public void setStatutoryDeductions(Set<StatutoryDeduction> statutoryDeductions) {
        this.statutoryDeductions = statutoryDeductions;
    }
            
            
            

}
