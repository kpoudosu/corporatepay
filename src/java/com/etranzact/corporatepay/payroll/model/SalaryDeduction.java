/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKER_SALARY")
@Audited
public class SalaryDeduction implements Serializable {
    @ManyToOne
    @JoinColumn(name = "WORKERS_SALARY_ID")
    private WorkerSalary workerSalary;

    @Id
    @GeneratedValue
    @Column(name = "SALARY_DEDUCTION_ID")
    private Long Id;
    @ManyToOne
    @JoinColumn(name = "WORKER_ID")
    private Worker worker;
      @Column(name = "AMOUNT")
    private BigDecimal amount = new BigDecimal("0.0");
    @ManyToOne
    @JoinColumn(name = "WORKER_DEDUCTION")
    private WorkersDeduction workersDeduction;

    /**
     * @return the workerSalary
     */
    public WorkerSalary getWorkerSalary() {
        return workerSalary;
    }

    /**
     * @param workerSalary the workerSalary to set
     */
    public void setWorkerSalary(WorkerSalary workerSalary) {
        this.workerSalary = workerSalary;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the worker
     */
    public Worker getWorker() {
        return worker;
    }

    /**
     * @param worker the worker to set
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the workersDeduction
     */
    public WorkersDeduction getWorkersDeduction() {
        return workersDeduction;
    }

    /**
     * @param workersDeduction the workersDeduction to set
     */
    public void setWorkersDeduction(WorkersDeduction workersDeduction) {
        this.workersDeduction = workersDeduction;
    }
    
    
      

}
