/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.StatutoryDeductionAuthority;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_STATUTORY_DEDUCTION", uniqueConstraints = {@UniqueConstraint(columnNames = {"DEDUCTION_NAME"})})
@Audited
public class StatutoryDeduction implements Serializable {

    @Id
      @GeneratedValue
    @Column(name = "STATUTORY_DEDUCTION_ID")
    private Long Id;
    @ManyToOne
    @JoinColumn(name="STATUTORY_DEDUCTION_AUTH")
    private StatutoryDeductionAuthority statutoryDeductionAuthority;
      @ManyToOne
    @JoinColumn(name="TMP_STATUTORY_DEDUCTION_AUTH")
    private StatutoryDeductionAuthority tmpStatutoryDeductionAuthority;
       @Column(name="DEDUCTION_NAME")
    private String deductionName;
             @Column(name="TMP_DEDUCTION_NAME")
    private String tmpDeductionName;
            @Column(name="AMOUNT_TYPE")
    private String amountType;
                 @Column(name="TMP_AMOUNT_TYPE")
    private String tmpAmountType;
        @Column(name="AMOUNT")
     private BigDecimal amount =new BigDecimal("0.0");
                @Column(name="TMP_AMOUNT")
     private BigDecimal tmpAmount =new BigDecimal("0.0");
                  @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
           @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
    


    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the statutoryDeductionAuthority
     */
    public StatutoryDeductionAuthority getStatutoryDeductionAuthority() {
        return statutoryDeductionAuthority;
    }

    /**
     * @param statutoryDeductionAuthority the statutoryDeductionAuthority to set
     */
    public void setStatutoryDeductionAuthority(StatutoryDeductionAuthority statutoryDeductionAuthority) {
        this.statutoryDeductionAuthority = statutoryDeductionAuthority;
    }

    /**
     * @return the tmpStatutoryDeductionAuthority
     */
    public StatutoryDeductionAuthority getTmpStatutoryDeductionAuthority() {
        return tmpStatutoryDeductionAuthority;
    }

    /**
     * @param tmpStatutoryDeductionAuthority the tmpStatutoryDeductionAuthority to set
     */
    public void setTmpStatutoryDeductionAuthority(StatutoryDeductionAuthority tmpStatutoryDeductionAuthority) {
        this.tmpStatutoryDeductionAuthority = tmpStatutoryDeductionAuthority;
    }

    /**
     * @return the deductionName
     */
    public String getDeductionName() {
        return deductionName;
    }

    /**
     * @param deductionName the deductionName to set
     */
    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the tmpDeductionName
     */
    public String getTmpDeductionName() {
        return tmpDeductionName;
    }

    /**
     * @param tmpDeductionName the tmpDeductionName to set
     */
    public void setTmpDeductionName(String tmpDeductionName) {
        this.tmpDeductionName = tmpDeductionName;
    }

    /**
     * @return the tmpAmountType
     */
    public String getTmpAmountType() {
        return tmpAmountType;
    }

    /**
     * @param tmpAmountType the tmpAmountType to set
     */
    public void setTmpAmountType(String tmpAmountType) {
        this.tmpAmountType = tmpAmountType;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }
    
    

}
