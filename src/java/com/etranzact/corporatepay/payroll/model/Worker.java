/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKER")
@Audited
public class Worker extends Beneficiary{

         @ManyToOne
         @JoinColumn(name="WORKERS_CATEGORY_ID")
            @Audited(targetAuditMode = NOT_AUDITED)
     private WorkersCategory workersCategory;
             @ManyToOne
         @JoinColumn(name="TMP_WORKERS_CATEGORY_ID")
                @Audited(targetAuditMode = NOT_AUDITED)
     private WorkersCategory tmpWorkersCategory;
                      @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
           @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
       @JoinColumn(name = "PAYROLL_PERIOD_INCLUDED")
    @ManyToOne
    private PayrollPeriod includedPayrollPeriod;

    /**
     * @return the workersCategory
     */
    public WorkersCategory getWorkersCategory() {
        return workersCategory;
    }

    /**
     * @param workersCategory the workersCategory to set
     */
    public void setWorkersCategory(WorkersCategory workersCategory) {
        this.workersCategory = workersCategory;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the tmpWorkersCategory
     */
    public WorkersCategory getTmpWorkersCategory() {
        return tmpWorkersCategory;
    }

    /**
     * @param tmpWorkersCategory the tmpWorkersCategory to set
     */
    public void setTmpWorkersCategory(WorkersCategory tmpWorkersCategory) {
        this.tmpWorkersCategory = tmpWorkersCategory;
    }

    /**
     * @return the includedPayrollPeriod
     */
    public PayrollPeriod getIncludedPayrollPeriod() {
        return includedPayrollPeriod;
    }

    /**
     * @param includedPayrollPeriod the includedPayrollPeriod to set
     */
    public void setIncludedPayrollPeriod(PayrollPeriod includedPayrollPeriod) {
        this.includedPayrollPeriod = includedPayrollPeriod;
    }

   

}
