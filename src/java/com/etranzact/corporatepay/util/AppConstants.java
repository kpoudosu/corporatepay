/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
public interface AppConstants {

    public static final String LOGIN_USER = "loginUser";
    public static final String LOGIN_PAGE = "/login.jsf";
    public static final String DASHBOARD_PAGE = "/dashboard.jsf";

    //flag statuses
    public static final String CREATED = "CR";
    public static final String MODIFIED = "MD";
    public static final String APPROVED = "AP";
    public static final String REJECTED = "RJ";
    public static final String KEPT_IN_VIEW = "KIV";

    //accesstypes
    public static final String ADMINISTRATOR_ACCESS = "ADA";
    public static final String BANK_ADMINISTRATOR_ACCESS = "BADA";
    public static final String CORPORATE_ADMINISTRATOR_ACCESS = "CADA";
    public static final String ADMINISTRATOR_BANKADMINISTRATOR_ACCESS = "ADA_BADA";
    public static final String ADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS = "ADA_CADA";
    public static final String BANKADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS = "BADA_CADA";
    public static final String ALL_ROLE_TYPE_ACCESS = "ARTA";
     public static final String NO_ACCESS = "NA";

    //transactiontypes
    public static final String SALARY_PAYMENT_TRANSACTION_TYPE = "SPTT";
    public static final String COMPLETE_SALARY_PAYMENT_TRANSACTION_TYPE = "CSPTT";
    public static final String VENDOR_PAYMENT_TRANSACTION_TYPE = "VPTT";
    public static final String PENSION_PAYMENT_TRANSACTION_TYPE = "PPTT";
    public static final String TAX_PAYMENT_TRANSACTION_TYPE = "TPTT";
    public static final String PAYROLL_PAYMENT_TRANSACTION_TYPE = "PRPTT";
    public static final String SMS_CHARGE_TRANSACTION_TYPE = "SCTT";
    public static final String INIDIVIDUAL_TRANSACTION_TYPE = "INTT";
    
   //task access level
      public static final String USER_TASK_TYPE = "User";
            public static final String GROUP_TASK_TYPE = "Group";
    
            
            //turn on and off
                    public static final String TURNED_ON = "0";
                            public static final String TURNED_OFF = "0";
            
            

}
