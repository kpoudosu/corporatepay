/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;


import com.etranzact.corporatepay.app.LocationManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author oluwasegun.idowu
 */
public class ConfigurationUtil {

    private static ConfigurationUtil util;
    private String applicationDir;
    private Properties mainProperties;
    private Properties mailProperties;
    public final String CONFIG_DIR_NAME = "corporatepay\\config";
        private static final Logger logger = Logger.getLogger(ConfigurationUtil.class.getName());
        private int loginRetrialAllowed = 3;

    public static ConfigurationUtil instance() {
        if (util == null) {
            util = new ConfigurationUtil();
        }
        return util;
    }

    public void init(ServletContext context) throws Exception {
        findHomeDirectory(context);
        loadProperties();
    }

    private void findHomeDirectory(ServletContext context) throws Exception {
        String configPath = context.getRealPath("/");
        int tmpIndex = configPath.lastIndexOf(File.separator + "tmp" + File.separator);
        if (tmpIndex > 0) {
            configPath = configPath.substring(0, tmpIndex) + File.separator + CONFIG_DIR_NAME;
        }
        if (new File(configPath).isDirectory()) {
            applicationDir = configPath;
            logger.log(Level.INFO, "Application Directory {0}", applicationDir);
        } else {
              logger.log(Level.SEVERE,"Application Directory Not Found. Contact the Administrator");
            throw new Exception("Application Directory Not Found. Contact the Administrator");

        }
    }

    private void loadProperties() throws IOException {
        mainProperties = readConfig("main.properties");
        mailProperties = readConfig("mail.properties");
    }

    /**
     * @return the applicationDir
     */
    public String getApplicationDir() {
        return applicationDir;
    }

    /**
     * @return the mainProperties
     */
    public Properties getMainProperties() {
        return mainProperties;
    }

    /**
     * @return the mailProperties
     */
    public Properties getMailProperties() {
        return mailProperties;
    }

    private Properties readConfig(String configFile) throws IOException {
        File file = new File(applicationDir + File.separator + configFile);
        String[] split = configFile.split("\\.");
        String ext = split[split.length - 1];
        Properties properties = null;
        switch (ext) {
            case "properties":

                if (file.exists()) {
                    properties = new Properties();
                    properties.load(new FileInputStream(file));
                    return properties;
                }
                break;
        }

        return null;
    }

    public void releasePropertiesResource() {
        mainProperties = null;
        mailProperties = null;
    }

    /**
     * @return the loginRetrialAllowed
     */
    public int getLoginRetrialAllowed() {
        String property = mainProperties.getProperty("no-of-login-retrial-allowed");
        try {
            loginRetrialAllowed = Integer.parseInt(property);
            return loginRetrialAllowed;
        }catch(NumberFormatException ex) {
        return loginRetrialAllowed;
        }
    }


    
}
