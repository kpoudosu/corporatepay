/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.listener;

import com.etranzact.corporatepay.app.LocationManager;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.AuditTable;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.envers.RevisionListener;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
public class AuditTableListener implements RevisionListener {
    @Inject
    private LocationManager locationManager;

    @Override
    public void newRevision(Object o) {
        AuditTable auditTable = (AuditTable) o;
        
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            Approver loginUser = (Approver) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            auditTable.setUsername(loginUser.getId());

        } catch (Exception e) {
            //e.printStackTrace();
        }

        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
            auditTable.setLocation(locationManager.retrieveDetails(request));
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
    
}
