/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_COMMISSION")
@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class Commission implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "COMMISSION_ID")
    private Long id;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_COMMISSION_COMMISSIONFEE", joinColumns
            = @JoinColumn(name = "COMMISSION_ID"), inverseJoinColumns
            = @JoinColumn(name = "COMMISSION_FEE_ID"))
    private Set<CommissionFee> commissionFees = new HashSet<>();
    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_COMMISSION_COMMISSIONFEE", joinColumns
            = @JoinColumn(name = "COMMISSION_ID"), inverseJoinColumns
            = @JoinColumn(name = "COMMISSION_FEE_ID"))
    private Set<CommissionFee> tmpCommissionFees;
//    @ManyToOne
//    @JoinColumn(name = "TRANS_TYPE_ID")
//       @Audited(targetAuditMode = NOT_AUDITED)
//    private TransactionType transactionType;
//       @ManyToOne
//    @JoinColumn(name = "TMP_TRANS_TYPE_ID")
//          @Audited(targetAuditMode = NOT_AUDITED)
//    private TransactionType tempTransactionType;
    @Column(name = "TRANSACTION_TYPE")
    private String transactionType;
        @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
           @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (object instanceof Commission) {
            Commission rhs = (Commission) object;
            return new EqualsBuilder()
                    .appendSuper(super.equals(object))
                    .append(getId(), rhs.getId())
                    .isEquals();
        } else {
            return false;
        }

    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
                append("id", getId()).
                toString();
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the commissionFees
     */
    public Set<CommissionFee> getCommissionFees() {
        return commissionFees;
    }

    /**
     * @param commissionFees the commissionFees to set
     */
    public void setCommissionFees(Set<CommissionFee> commissionFees) {
        this.commissionFees = commissionFees;
    }

    /**
     * @return the transactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the tmpCommissionFees
     */
    public Set<CommissionFee> getTmpCommissionFees() {
        return tmpCommissionFees;
    }

    /**
     * @param tmpCommissionFees the tmpCommissionFees to set
     */
    public void setTmpCommissionFees(Set<CommissionFee> tmpCommissionFees) {
        this.tmpCommissionFees = tmpCommissionFees;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

 

}
