/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_CORPORATE", uniqueConstraints = {@UniqueConstraint(columnNames = {"CORPORATE_NAME","BANK_ID"})})
@Audited
public class Corporate implements Serializable {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Long id;
    @Column(name="CORPORATE_NAME")
    private String corporateName;
    @ManyToOne
    @JoinColumn(name="BANK_ID")
    private Bank bank = new Bank();
    @Column(name="CORPORATE_EMAIL")
    private String contactEmail;
    @Column(name="CORPORATE_STREET")
    private String contactStreet;
    @Column(name="CORPORATE_TOWN")
    private String contactTown;
    @Column(name="CORPORATE_STATE")
    private String contactState;
    @Column(name="CORPORATE_PHONE")
    private String contactPhone;
      @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
         @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_CORPORATE_CORPORATEPAY_SERVICE", joinColumns =
    @JoinColumn(name = "CORPORATE_ID"), inverseJoinColumns =
    @JoinColumn(name = "SERVICE_ID"))
    private Set<CorporatePayService> corporatePayServices = new HashSet<>();
                  @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_CORPORATE_CORPORATEPAY_SERVICE", joinColumns =
    @JoinColumn(name = "CORPORATE_ID"), inverseJoinColumns =
    @JoinColumn(name = "SERVICE_ID"))
    private Set<CorporatePayService> tmpCorporatePayServices = new HashSet<>();
                      @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
    
    @Lob
    private byte[] logo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactStreet() {
        return contactStreet;
    }

    public void setContactStreet(String contactStreet) {
        this.contactStreet = contactStreet;
    }

    public String getContactTown() {
        return contactTown;
    }

    public void setContactTown(String contactTown) {
        this.contactTown = contactTown;
    }

    public String getContactState() {
        return contactState;
    }

    public void setContactState(String contactState) {
        this.contactState = contactState;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public byte[] getLogo() {
        return logo;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporatePayServices
     */
    public Set<CorporatePayService> getCorporatePayServices() {
        return corporatePayServices;
    }

    /**
     * @param corporatePayServices the corporatePayServices to set
     */
    public void setCorporatePayServices(Set<CorporatePayService> corporatePayServices) {
        this.corporatePayServices = corporatePayServices;
    }

    /**
     * @return the tmpCorporatePayServices
     */
    public Set<CorporatePayService> getTmpCorporatePayServices() {
        return tmpCorporatePayServices;
    }

    /**
     * @param tmpCorporatePayServices the tmpCorporatePayServices to set
     */
    public void setTmpCorporatePayServices(Set<CorporatePayService> tmpCorporatePayServices) {
        this.tmpCorporatePayServices = tmpCorporatePayServices;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(byte[] logo) {
        this.logo = logo;
    }


    
    
}
