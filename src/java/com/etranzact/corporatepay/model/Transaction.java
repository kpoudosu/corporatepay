/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_TRANSACTION")
@Audited
public class Transaction implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "TRANSACTION_ID")
    private Long transactionId;
    @ManyToOne
    @JoinColumn(name = "CORPORATE_ID")
    private Corporate corporate = new Corporate();
    @ManyToOne
    @JoinColumn(name = "BENEFICIARY_ID")
    private Beneficiary beneficiary;
    @ManyToOne
    @JoinColumn(name = "SOURCE_CARD")
    private Card card;
    @Column(name = "TRANSACTION_TYPE")
    private String trsnsactionType;
    @Column(name = "TRANSACTION_STATUS")
    private String trsnsactionStatus;
                 @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();

    /**
     * @return the transactionId
     */
    public Long getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the beneficiary
     */
    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    /**
     * @param beneficiary the beneficiary to set
     */
    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    /**
     * @return the card
     */
    public Card getCard() {
        return card;
    }

    /**
     * @param card the card to set
     */
    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * @return the trsnsactionType
     */
    public String getTrsnsactionType() {
        return trsnsactionType;
    }

    /**
     * @param trsnsactionType the trsnsactionType to set
     */
    public void setTrsnsactionType(String trsnsactionType) {
        this.trsnsactionType = trsnsactionType;
    }

    /**
     * @return the trsnsactionStatus
     */
    public String getTrsnsactionStatus() {
        return trsnsactionStatus;
    }

    /**
     * @param trsnsactionStatus the trsnsactionStatus to set
     */
    public void setTrsnsactionStatus(String trsnsactionStatus) {
        this.trsnsactionStatus = trsnsactionStatus;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    
}
