/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_BANK_ACCOUNT")
@Audited
public class BankAccount extends Account {

    @Column(name = "ACCOUNT_NAME", nullable = false)
    private String accountName;
        @Column(name = "TMP_ACCOUNT_NAME", nullable = false)
    private String tmpAccountName;
    @ManyToOne
    @JoinColumn(name = "BANK_ID")
    private Bank bank = new Bank();
     @ManyToOne
    @JoinColumn(name = "TMP_BANK_ID")
    private Bank tmpBank;
    @Column(name = "ACCOUNT_NUMBER")
    @Size(min = 10, max = 10)
    private String accountNumber;
       @Column(name = "TMP_ACCOUNT_NUMBER")
    private String tmpAccountNumber;
    @Column(name = "ACCOUNT_TYPE")
    private String accountType;
       @Column(name = "TMP_ACCOUNT_TYPE")
    private String tmpAccountType;


    

    /**
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * @param accountName the accountName to set
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     * @return the tmpAccountName
     */
    public String getTmpAccountName() {
        return tmpAccountName;
    }

    /**
     * @param tmpAccountName the tmpAccountName to set
     */
    public void setTmpAccountName(String tmpAccountName) {
        this.tmpAccountName = tmpAccountName;
    }

    /**
     * @return the tmpBank
     */
    public Bank getTmpBank() {
        return tmpBank;
    }

    /**
     * @param tmpBank the tmpBank to set
     */
    public void setTmpBank(Bank tmpBank) {
        this.tmpBank = tmpBank;
    }

    /**
     * @return the tmpAccountNumber
     */
    public String getTmpAccountNumber() {
        return tmpAccountNumber;
    }

    /**
     * @param tmpAccountNumber the tmpAccountNumber to set
     */
    public void setTmpAccountNumber(String tmpAccountNumber) {
        this.tmpAccountNumber = tmpAccountNumber;
    }

    /**
     * @return the tmpAccountType
     */
    public String getTmpAccountType() {
        return tmpAccountType;
    }

    /**
     * @param tmpAccountType the tmpAccountType to set
     */
    public void setTmpAccountType(String tmpAccountType) {
        this.tmpAccountType = tmpAccountType;
    }
    

    
}
