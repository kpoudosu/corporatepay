/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_ROLE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ROLE_NAME"})})
@Audited
public class Role implements Serializable {
    private static long serialVersionUID = 1L;

   
    @Id
    @Column(name="ROLE_ID", nullable = false, length = 20)
    private String id;
    @Column(name = "ROLE_NAME", nullable = false, length = 50)
    private String roleName;
          @Column(name="ACCESS_LEVEL", length = 10)
    private String accessLevel;
    @Column(name="ADMIN_ROLE")
    private Boolean administrativeRole = Boolean.FALSE;
    @Column(name="BANK_ROLE")
    private Boolean bankRole = Boolean.FALSE;
    @Column(name="MERCHANT_ROLE")
    private Boolean merchantRole = Boolean.FALSE;
    @Column(name="SUPER_ADMIN_ROLE")
    private Boolean superAdministrativeRole = Boolean.FALSE;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_ROLETABLE_MENUITEM", joinColumns =
    @JoinColumn(name = "ROLE_ID"), inverseJoinColumns =
    @JoinColumn(name = "MENUITEM_ID"))
    private Set<MenuItem> resources = new HashSet<>();
        @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Boolean getAdministrativeRole() {
        return administrativeRole;
    }

    public void setAdministrativeRole(Boolean administrativeRole) {
        this.administrativeRole = administrativeRole;
    }

    public Boolean getBankRole() {
        return bankRole;
    }

    public void setBankRole(Boolean bankRole) {
        this.bankRole = bankRole;
    }

    public Boolean getMerchantRole() {
        return merchantRole;
    }

    public void setMerchantRole(Boolean merchantRole) {
        this.merchantRole = merchantRole;
    }

    public Boolean getSuperAdministrativeRole() {
        return superAdministrativeRole;
    }

    public void setSuperAdministrativeRole(Boolean superAdministrativeRole) {
        this.superAdministrativeRole = superAdministrativeRole;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Set<MenuItem> getResources() {
        return resources;
    }

    public void setResources(Set<MenuItem> resources) {
        this.resources = resources;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.etranzact.model.Role[ id=" + getId() + " ]";
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the accessLevel
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }
    
    
    
}
