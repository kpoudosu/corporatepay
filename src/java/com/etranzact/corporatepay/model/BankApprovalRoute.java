/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_BANK_APPROVALROUTE", uniqueConstraints = {@UniqueConstraint(columnNames = {"BANK_ID"})})
@Audited
public class BankApprovalRoute extends ApprovalRoute {


    @ManyToOne
    @JoinColumn(name="BANK_ID")
    private Bank bank = new Bank();
    
       @ManyToOne
    @JoinColumn(name="TMP_BANK_ID")
    private Bank tmpBank;
       
         public BankApprovalRoute() {
        setCentral(Boolean.FALSE);
    }
   
    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the tmpBank
     */
    public Bank getTmpBank() {
        return tmpBank;
    }

    /**
     * @param tmpBank the tmpBank to set
     */
    public void setTmpBank(Bank tmpBank) {
        this.tmpBank = tmpBank;
    }
    
    
    
}
