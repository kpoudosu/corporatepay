/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_USER")
@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class User extends Approver implements Serializable {
    @ManyToMany(mappedBy = "users")
    private List<ApprovalGroup> approvalGroups;

    @Column(name="USERNAME", unique = true, nullable = false)
    private String username;
    @Column(name = "PASSWORD",nullable = false)
    private String password;
    @Column(name = "SCRET_KEY",nullable = false)
    private String securityKey;
    @JoinColumn(name = "ROLE_ID")
    @ManyToOne
    private Role role = new Role();
    @JoinColumn(name = "TMP_ROLE_ID")
    @ManyToOne
    private Role tmpRole;
    @Column(name = "FIRSTNAME")
    private String firstName;
    @Column(name = "LASTNAME")
    private String lastName;
    @Column(name = "TMP_FIRSTNAME")
    private String tmpFirstName;
    @Column(name = "TMP_LASTNAME")
    private String tmpLastName;
    @Email
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "TMP_EMAIL")
    private String tmpEmail;
    @Column(name = "MOBILE_PHONE",  length = 200)
    private String mobilePhone;
    @Column(name = "TMP_MOBILE_PHONE",  length = 200)
    private String tmpMobilePhone;
    
 
   
    
//    @Column(name = "CHANGE_PASSWORD")
    @Transient
    private Boolean changePassword = Boolean.FALSE;
 //   @Column(name = "PASS_EXPIRE")
    @Transient
    private Boolean passwordExpired = Boolean.FALSE;
    
    @Column(name = "PASS_LOCK_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passwordLockDate;


    @Column(name = "PASS_DATE_EXPIRE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passwordDateExpired;
    
//    @Column(name = "USER_DISABLED")
     @Transient
    private Boolean userDisabled = Boolean.FALSE;
//    @Column(name = "USER_LOCKED")
    @Transient
    private Boolean userLocked = Boolean.FALSE;
    
    @Column(name = "PASSWORD_MISSED")
    private Integer passwordMissed = 0;
    
    @Column(name = "DAY_1",  length = 1)
    private String monday = AppConstants.TURNED_OFF;
    @Column(name = "DAY_2",  length = 1)
    private String tuesday = AppConstants.TURNED_OFF;
    @Column(name = "DAY_3",  length = 1)
    private String wednesday = AppConstants.TURNED_OFF;
    @Column(name = "DAY_4",  length = 1)
    private String thursday = AppConstants.TURNED_OFF;
    @Column(name = "DAY_5",  length = 1)
    private String friday = AppConstants.TURNED_OFF;
    @Column(name = "DAY_6",  length = 1)
    private String saturday = AppConstants.TURNED_OFF;
    @Column(name = "DAY_7", length = 1)
    private String sunday = AppConstants.TURNED_OFF;
    
    @Column(name = "TMP_DAY_1",  length = 1)
    private String tmpMonday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_2",  length = 1)
    private String tmpTuesday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_3",  length = 1)
    private String tmpWednesday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_4",  length = 1)
    private String tmpThursday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_5",  length = 1)
    private String tmpFriday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_6",  length = 1)
    private String tmpSaturday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_7", length = 1)
    private String tmpSunday = AppConstants.TURNED_OFF;
    
    @Column(name = "FROM_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromTimeAccess;
    @Column(name = "TO_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toTimeAccess;
    
    @Column(name = "TMP_FROM_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tmpFromTimeAccess;
    @Column(name = "TMP_TO_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tmpToTimeAccess;
        @Column(name="ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;
       @Column(name="TMP_ACTIVE_STATUS")
    private Boolean tmpActive = Boolean.FALSE;
     @Column(name = "FLAG_STATUS")
    private String flagStatus;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATE_CREATED", updatable = false)
    private Date createdDate = new Date();
    @Transient
    private String bankCode;
       @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;

    @Override
    public String getIdentity() {
       return firstName + " " + lastName + "(USER)"; 
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getTmpRole() {
        return tmpRole;
    }

    public void setTmpRole(Role tmpRole) {
        this.tmpRole = tmpRole;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTmpFirstName() {
        return tmpFirstName;
    }

    public void setTmpFirstName(String tmpFirstName) {
        this.tmpFirstName = tmpFirstName;
    }

    public String getTmpLastName() {
        return tmpLastName;
    }

    public void setTmpLastName(String tmpLastName) {
        this.tmpLastName = tmpLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTmpEmail() {
        return tmpEmail;
    }

    public void setTmpEmail(String tmpEmail) {
        this.tmpEmail = tmpEmail;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getTmpMobilePhone() {
        return tmpMobilePhone;
    }

    public void setTmpMobilePhone(String tmpMobilePhone) {
        this.tmpMobilePhone = tmpMobilePhone;
    }

   
    public Boolean getChangePassword() {
        changePassword = passwordExpired;
        return changePassword;
    }

    public void setChangePassword(Boolean changePassword) {
        this.changePassword = changePassword;
    }

    public Boolean getPasswordExpired() {
        passwordExpired = Calendar.getInstance().getTimeInMillis()>passwordDateExpired.getTime();
        return passwordExpired;
    }

    public void setPasswordExpired(Boolean passwordExpired) {
        this.passwordExpired = passwordExpired;
    }

    public Boolean getUserDisabled() {
        userDisabled = !active;
        return userDisabled;
    }

    public void setUserDisabled(Boolean userDisabled) {
        this.userDisabled = userDisabled;
    }

    public Boolean getUserLocked() {
        userLocked = passwordMissed >=ConfigurationUtil.instance().getLoginRetrialAllowed();
        return userLocked;
    }

    public void setUserLocked(Boolean userLocked) {
        this.userLocked = userLocked;
    }

    public Integer getPasswordMissed() {
        return passwordMissed;
    }

    public void setPasswordMissed(Integer passwordMissed) {
        this.passwordMissed = passwordMissed;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getTmpMonday() {
        return tmpMonday;
    }

    public void setTmpMonday(String tmpMonday) {
        this.tmpMonday = tmpMonday;
    }

    public String getTmpTuesday() {
        return tmpTuesday;
    }

    public void setTmpTuesday(String tmpTuesday) {
        this.tmpTuesday = tmpTuesday;
    }

    public String getTmpWednesday() {
        return tmpWednesday;
    }

    public void setTmpWednesday(String tmpWednesday) {
        this.tmpWednesday = tmpWednesday;
    }

    public String getTmpThursday() {
        return tmpThursday;
    }

    public void setTmpThursday(String tmpThursday) {
        this.tmpThursday = tmpThursday;
    }

    public String getTmpFriday() {
        return tmpFriday;
    }

    public void setTmpFriday(String tmpFriday) {
        this.tmpFriday = tmpFriday;
    }

    public String getTmpSaturday() {
        return tmpSaturday;
    }

    public void setTmpSaturday(String tmpSaturday) {
        this.tmpSaturday = tmpSaturday;
    }

    public String getTmpSunday() {
        return tmpSunday;
    }

    public void setTmpSunday(String tmpSunday) {
        this.tmpSunday = tmpSunday;
    }

    public Date getFromTimeAccess() {
        return fromTimeAccess;
    }

    public void setFromTimeAccess(Date fromTimeAccess) {
        this.fromTimeAccess = fromTimeAccess;
    }

    public Date getToTimeAccess() {
        return toTimeAccess;
    }

    public void setToTimeAccess(Date toTimeAccess) {
        this.toTimeAccess = toTimeAccess;
    }

    public Date getTmpFromTimeAccess() {
        return tmpFromTimeAccess;
    }

    public void setTmpFromTimeAccess(Date tmpFromTimeAccess) {
        this.tmpFromTimeAccess = tmpFromTimeAccess;
    }

    public Date getTmpToTimeAccess() {
        return tmpToTimeAccess;
    }

    public void setTmpToTimeAccess(Date tmpToTimeAccess) {
        this.tmpToTimeAccess = tmpToTimeAccess;
    }


    public Date getPasswordLockDate() {
        return passwordLockDate;
    }

    public void setPasswordLockDate(Date passwordLockDate) {
        this.passwordLockDate = passwordLockDate;
    }

    public Date getPasswordDateExpired() {
        return passwordDateExpired;
    }

    public void setPasswordDateExpired(Date passwordDateExpired) {
        this.passwordDateExpired = passwordDateExpired;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the approvalGroups
     */
    public List<ApprovalGroup> getApprovalGroups() {
        return approvalGroups;
    }

    /**
     * @param approvalGroups the approvalGroups to set
     */
    public void setApprovalGroups(List<ApprovalGroup> approvalGroups) {
        this.approvalGroups = approvalGroups;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }
    
    
    
}
