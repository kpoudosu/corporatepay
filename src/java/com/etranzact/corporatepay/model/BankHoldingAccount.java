/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_BANK_HOLDING_ACCOUNT", uniqueConstraints = {@UniqueConstraint(columnNames = {"BANK_ID"})})
@Audited
public class BankHoldingAccount extends HoldingAccount  {

    @OneToOne
    @JoinColumn(name="BANK_ID")
    private Bank bank = new Bank();
     
     @OneToOne
    @JoinColumn(name="TMP_BANK_ID")
    private Bank tmpBank;

            public BankHoldingAccount() {
        setCentral(Boolean.FALSE);
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the tmpBank
     */
    public Bank getTmpBank() {
        return tmpBank;
    }

    /**
     * @param tmpBank the tmpBank to set
     */
    public void setTmpBank(Bank tmpBank) {
        this.tmpBank = tmpBank;
    }

   
    
}
