/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.listener.AuditTableListener;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="AUDIT_REVISION_LOG")
@RevisionEntity(AuditTableListener.class)
public class AuditTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name="ID")
    @Id
    @GeneratedValue
    @RevisionNumber
    private Long id;

    @Column(name="REVISION_TIMESTAMP")
    @RevisionTimestamp
    private Date timestamp;

    @Column(name="USER_ID")
    private Long username;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="country",column = @Column(name="AUDIT_COUNTRY")),
            @AttributeOverride(name="ip",column = @Column(name="AUDIT_IP")),
            @AttributeOverride(name="city",column = @Column(name="AUDIT_CITY")),
            @AttributeOverride(name="latitude",column = @Column(name="AUDIT_LATITUDE")),
            @AttributeOverride(name="longitude",column = @Column(name="AUDIT_LONGITUDE"))
    })
    private Location location;
    
    @ElementCollection
    @JoinTable(name = "REVCHANGES", joinColumns = @JoinColumn(name = "REV"))
    @Column(name = "ENTITYNAME")
    @ModifiedEntityNames
    private Set<String> modifiedEntityNames;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUsername() {
        return username;
    }

    public void setUsername(Long username) {
        this.username = username;
    }

    public Set<String> getModifiedEntityNames() {
        return modifiedEntityNames;
    }

    public void setModifiedEntityNames(Set<String> modifiedEntityNames) {
        this.modifiedEntityNames = modifiedEntityNames;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuditTable)) {
            return false;
        }
        AuditTable other = (AuditTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.etranzact.model.AuditTable[ id=" + id + " ]";
    }
    
}
