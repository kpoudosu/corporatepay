/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_BANK_APPROVAL_GROUP")
@Audited
public class BankApprovalGroup extends ApprovalGroup {
    
     
    @JoinColumn(name = "BANK_ID")
    @ManyToOne
    private Bank bank;
    @JoinColumn(name = "TMP_BANK_ID")
    @ManyToOne
    private Bank tmpBank;
    
    public BankApprovalGroup() {
        setCentral(Boolean.FALSE);
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the tmpBank
     */
    public Bank getTmpBank() {
        return tmpBank;
    }

    /**
     * @param tmpBank the tmpBank to set
     */
    public void setTmpBank(Bank tmpBank) {
        this.tmpBank = tmpBank;
    }
    
    
}
