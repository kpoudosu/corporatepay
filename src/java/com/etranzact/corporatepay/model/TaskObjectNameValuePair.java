/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Entity
@Audited
@Table(name="COP_TASK_OBJECT")
public class TaskObjectNameValuePair implements Serializable{
    @ManyToOne
    private ApprovalTask approvalTask;
    
    private String name;
    private String targetValue;
    
           @Id
    @Column(name="ID")
           @GeneratedValue
    private Long Id;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the targetValue
     */
    public String getTargetValue() {
        return targetValue;
    }

    /**
     * @param targetValue the targetValue to set
     */
    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the approvalTask
     */
    public ApprovalTask getApprovalTask() {
        return approvalTask;
    }

    /**
     * @param approvalTask the approvalTask to set
     */
    public void setApprovalTask(ApprovalTask approvalTask) {
        this.approvalTask = approvalTask;
    }

   
    
    
}
