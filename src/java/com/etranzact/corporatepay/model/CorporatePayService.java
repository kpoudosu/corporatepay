/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_SERVICE")
public class CorporatePayService implements Serializable {
    @Id
    @Column(name="ID", length=6)
    private String serviceCode;
    @Column(name="SERVICE_DESC")
    private String serviceDesc;
     @Column(name="IS_DEFAULT")
    private Boolean defaultService = Boolean.TRUE;
                 @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();

    /**
     * @return the serviceCode
     */
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * @param serviceCode the serviceCode to set
     */
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    /**
     * @return the serviceDesc
     */
    public String getServiceDesc() {
        return serviceDesc;
    }

    /**
     * @param serviceDesc the serviceDesc to set
     */
    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    /**
     * @return the defaultService
     */
    public Boolean getDefaultService() {
        return defaultService;
    }

    /**
     * @param defaultService the defaultService to set
     */
    public void setDefaultService(Boolean defaultService) {
        this.defaultService = defaultService;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }


    
}
