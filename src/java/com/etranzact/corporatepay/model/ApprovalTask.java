/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_APPROVALTASK")
@Audited
public class ApprovalTask implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "APPROVAL_ROUTE_ID")
    private ApprovalRoute approvalRoute = new ApprovalRoute();
    @Column(name = "TARGET_OBJECT_ID")
    private String targetObjectId;
      @JoinColumn(name = "ACTOR_ID")
      @ManyToOne
    private Approver actor;
        @JoinColumn(name = "TASK_ACCESSOR")
      @ManyToOne
    private Approver taskAccessor;
        @Column(name = "COMMENT")
    private String comment;
    @Column(name = "APPROVAL_ACTION")
    private String approvalAction;
    @Lob
    private byte[] document;
        @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
            @Temporal(TemporalType.TIMESTAMP)
    @Column(name="ACTION_DATE")
    private Date actionDate = new Date();
    @Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(mappedBy = "approvalTask",cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<TaskObjectNameValuePair> nameValuePairs = new HashSet<>();
    @Transient
    private String taskType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    

    /**
     * @return the approvalRoute
     */
    public ApprovalRoute getApprovalRoute() {
        return approvalRoute;
    }

    /**
     * @param approvalRoute the approvalRoute to set
     */
    public void setApprovalRoute(ApprovalRoute approvalRoute) {
        this.approvalRoute = approvalRoute;
    }

    /**
     * @return the targetObjectId
     */
    public String getTargetObjectId() {
        return targetObjectId;
    }

    /**
     * @param targetObjectId the targetObjectId to set
     */
    public void setTargetObjectId(String targetObjectId) {
        this.targetObjectId = targetObjectId;
    }



    /**
     * @return the document
     */
    public byte[] getDocument() {
        return document;
    }


    /**
     * @return the approvalAction
     */
    public String getApprovalAction() {
        return approvalAction;
    }

    /**
     * @param approvalAction the approvalAction to set
     */
    public void setApprovalAction(String approvalAction) {
        this.approvalAction = approvalAction;
    }


    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }


    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the actionDate
     */
    public Date getActionDate() {
        return actionDate;
    }

    /**
     * @param actionDate the actionDate to set
     */
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    /**
     * @return the nameValuePairs
     */
    public Set<TaskObjectNameValuePair> getNameValuePairs() {
        return nameValuePairs;
    }

    /**
     * @param nameValuePairs the nameValuePairs to set
     */
    public void setNameValuePairs(Set<TaskObjectNameValuePair> nameValuePairs) {
        this.nameValuePairs = nameValuePairs;
    }

    /**
     * @return the actor
     */
    public Approver getActor() {
        return actor;
    }

    /**
     * @param actor the actor to set
     */
    public void setActor(Approver actor) {
        this.actor = actor;
    }

    /**
     * @return the taskAccessor
     */
    public Approver getTaskAccessor() {
        return taskAccessor;
    }

    /**
     * @param taskAccessor the taskAccessor to set
     */
    public void setTaskAccessor(Approver taskAccessor) {
        this.taskAccessor = taskAccessor;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(byte[] document) {
        this.document = document;
    }

    /**
     * @return the taskType
     */
    public String getTaskType() {
        return taskType;
    }

    /**
     * @param taskType the taskType to set
     */
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }


    
    
}
