/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_PAYMENT")
@Audited
@Inheritance(strategy= InheritanceType.JOINED)
public class Payment implements Serializable {
    @Id
    @Column(name = "PAYMENT_ID")
    private String paymentReference;
       @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
          @Column(name = "FLAG_STATUS")
    private String flagStatus;
           @Column(name = "PAYMENT_ALIAS")
    private String paymentAlias;
             @Column(name = "TMP_PAYMENT_ALIAS")
    private String tmpPaymentAlias;
    @ManyToOne
    @JoinColumn(name = "BANK_COMMISSION_ID")
    private BankCommission commissionFees = new BankCommission();
    
      @ManyToOne
    @JoinColumn(name = "TMP_BANK_COMMISSION_ID")
    private BankCommission tmpCommissionFees;
      
                   @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }




    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @param commissionFees the commissionFees to set
     */
    public void setCommissionFees(BankCommission commissionFees) {
        this.commissionFees = commissionFees;
    }

    /**
     * @return the commissionFees
     */
    public BankCommission getCommissionFees() {
        return commissionFees;
    }

    /**
     * @return the tmpCommissionFees
     */
    public BankCommission getTmpCommissionFees() {
        return tmpCommissionFees;
    }

    /**
     * @param tmpCommissionFees the tmpCommissionFees to set
     */
    public void setTmpCommissionFees(BankCommission tmpCommissionFees) {
        this.tmpCommissionFees = tmpCommissionFees;
    }

    /**
     * @return the paymentAlias
     */
    public String getPaymentAlias() {
        return paymentAlias;
    }

    /**
     * @param paymentAlias the paymentAlias to set
     */
    public void setPaymentAlias(String paymentAlias) {
        this.paymentAlias = paymentAlias;
    }

    /**
     * @return the tmpPaymentAlias
     */
    public String getTmpPaymentAlias() {
        return tmpPaymentAlias;
    }

    /**
     * @param tmpPaymentAlias the tmpPaymentAlias to set
     */
    public void setTmpPaymentAlias(String tmpPaymentAlias) {
        this.tmpPaymentAlias = tmpPaymentAlias;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    
}
