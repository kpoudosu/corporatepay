/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_CARD", uniqueConstraints = {@UniqueConstraint(columnNames = {"CARD_NUMBER"})})
@Audited
@Inheritance(strategy= InheritanceType.JOINED)
public class Card implements Serializable  {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Long id;
    @Size(min=16, max=16)
    @Column(name="CARD_NUMBER")
    private String cardNumber;
    @Column(name="CARD_DESC")
    private String description;
    @Column(name="ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;
    
    @Column(name="TMP_CARD_NUMBER")
    private String tmpCardNumber;
    @Column(name="TMP_CARD_DESC")
    private String tmpDescription;

    @Column(name="TMP_ACTIVE_STATUS")
    private Boolean tmpActive = Boolean.FALSE;
     @Column(name = "FLAG_STATUS")
    private String flagStatus;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATE_CREATED", updatable = false)
    private Date createdDate = new Date();
     @Column(name="CENTRAL")
    private Boolean central = Boolean.TRUE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getTmpCardNumber() {
        return tmpCardNumber;
    }

    public void setTmpCardNumber(String tmpCardNumber) {
        this.tmpCardNumber = tmpCardNumber;
    }

    public String getTmpDescription() {
        return tmpDescription;
    }

    public void setTmpDescription(String tmpDescription) {
        this.tmpDescription = tmpDescription;
    }


    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getTmpActive() {
        return tmpActive;
    }

    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }
    
    
}
