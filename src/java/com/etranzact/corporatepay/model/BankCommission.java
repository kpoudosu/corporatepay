/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_BANK_COMMISSION")
@Audited
public class BankCommission extends Commission {

    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_COMMISSION_COMMISSIONFEE", joinColumns =
    @JoinColumn(name = "COMMISSION_ID"), inverseJoinColumns =
    @JoinColumn(name = "COMMISSION_FEE_ID"))
    private Set<CommissionFee> bankCommissionFees = new HashSet<>();
    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_COMMISSION_COMMISSIONFEE", joinColumns =
    @JoinColumn(name = "COMMISSION_ID"), inverseJoinColumns =
    @JoinColumn(name = "COMMISSION_FEE_ID"))
    private Set<CommissionFee> tmpBankCommissionFees = new HashSet<>();
    @ManyToOne
    @JoinColumn(name = "ETZ_COMMISSION_ID")
    private Commission etzCommission = new Commission();

     @ManyToOne
    @JoinColumn(name = "TMP_ETZ_COMMISSION_ID")
    private Commission tmpEtzCommission = new Commission();
    
             public BankCommission() {
        setCentral(Boolean.FALSE);
    }
             
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) { return false; }
        if (object == this) { return true; }
        if (object instanceof BankCommission) {
        	BankCommission rhs = (BankCommission) object;
            return new EqualsBuilder()
                    .appendSuper(super.equals(object))
                    .append(getId(), rhs.getId())
                    .isEquals();
        } else {
            return false;
        }

    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
                append("id", getId()).
                toString();
    }

    

    /**
     * @return the etzCommission
     */
    public Commission getEtzCommission() {
        return etzCommission;
    }

    /**
     * @param etzCommission the etzCommission to set
     */
    public void setEtzCommission(Commission etzCommission) {
        this.etzCommission = etzCommission;
    }

    /**
     * @return the bankCommissionFees
     */
    public Set<CommissionFee> getBankCommissionFees() {
        return bankCommissionFees;
    }

    /**
     * @param bankCommissionFees the bankCommissionFees to set
     */
    public void setBankCommissionFees(Set<CommissionFee> bankCommissionFees) {
        this.bankCommissionFees = bankCommissionFees;
    }

    /**
     * @return the tmpBankCommissionFees
     */
    public Set<CommissionFee> getTmpBankCommissionFees() {
        return tmpBankCommissionFees;
    }

    /**
     * @param tmpBankCommissionFees the tmpBankCommissionFees to set
     */
    public void setTmpBankCommissionFees(Set<CommissionFee> tmpBankCommissionFees) {
        this.tmpBankCommissionFees = tmpBankCommissionFees;
    }

    /**
     * @return the tmpEtzCommission
     */
    public Commission getTmpEtzCommission() {
        return tmpEtzCommission;
    }

    /**
     * @param tmpEtzCommission the tmpEtzCommission to set
     */
    public void setTmpEtzCommission(Commission tmpEtzCommission) {
        this.tmpEtzCommission = tmpEtzCommission;
    }
    
    
}
