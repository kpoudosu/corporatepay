/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_CORPORATE_HOLDING_ACCOUNT", uniqueConstraints = {@UniqueConstraint(columnNames = {"CORPORATE_ID"})})
@Audited
public class CorporateHoldingAccount extends HoldingAccount  {
   
    @ManyToOne
    @JoinColumn(name="CORPORATE_ID")
    private Corporate corporate = new Corporate();
    @ManyToOne
    @JoinColumn(name="TMP_CORPORATE_ID")
    private Corporate tmpCorporate;
   
             public CorporateHoldingAccount() {
         setCentral(Boolean.FALSE);
     }


    public Corporate getCorporate() {
        return corporate;
    }

    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

   

    
}
