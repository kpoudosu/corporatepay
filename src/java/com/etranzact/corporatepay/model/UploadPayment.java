/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_UPLOAD_PAYMENT")
@Audited
public class UploadPayment extends Payment {

    @Column(name = "UPLOAD_PATH")
    private String uploadPath;
    @Column(name = "TMP_UPLOAD_PATH")
    private String tmpUploadPath;
    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "UPLOAD_TYPE_ID")
    private UploadType uploadType;

    /**
     * @return the uploadPath
     */
    public String getUploadPath() {
        return uploadPath;
    }

    /**
     * @param uploadPath the uploadPath to set
     */
    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    /**
     * @return the tmpUploadPath
     */
    public String getTmpUploadPath() {
        return tmpUploadPath;
    }

    /**
     * @param tmpUploadPath the tmpUploadPath to set
     */
    public void setTmpUploadPath(String tmpUploadPath) {
        this.tmpUploadPath = tmpUploadPath;
    }

    /**
     * @return the uploadType
     */
    public UploadType getUploadType() {
        return uploadType;
    }

    /**
     * @param uploadType the uploadType to set
     */
    public void setUploadType(UploadType uploadType) {
        this.uploadType = uploadType;
    }
    
    

}
