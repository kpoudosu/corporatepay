/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_BENEFICIARY", uniqueConstraints = {@UniqueConstraint(columnNames = {"BENEFCIARY_NAME","BENEFCIARY_EMAIL","BENEFCIARY_PHONE"})})
@Audited
@Inheritance(strategy= InheritanceType.JOINED)
public class Beneficiary implements Serializable {
    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "BENEFCIARY_NAME", nullable = false)
    private String beneficiaryName;
        @Column(name = "TMP_BENEFCIARY_NAME")
    private String tmpBeneficiaryName;
        @Email
       @Column(name = "BENEFCIARY_EMAIL", nullable = false)
    private String beneficiaryEmail;
        @Column(name = "TMP_BENEFCIARY_EMAIL")
    private String tmpBeneficiaryEmail;
       @Column(name = "BENEFCIARY_PHONE", nullable = false)
    private String beneficiaryPhone;
        @Column(name = "TMP_BENEFCIARY_PHONE")
    private String tmpBeneficiaryPhone;
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account = new Account();
     @ManyToOne
    @JoinColumn(name = "TMP_ACCOUNT_ID")
    private Account tmpAccount;
     @Transient
     private BigDecimal amount= new BigDecimal("0.0");
         @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
         @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the beneficiaryName
     */
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    /**
     * @param beneficiaryName the beneficiaryName to set
     */
    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    /**
     * @return the tmpBeneficiaryName
     */
    public String getTmpBeneficiaryName() {
        return tmpBeneficiaryName;
    }

    /**
     * @param tmpBeneficiaryName the tmpBeneficiaryName to set
     */
    public void setTmpBeneficiaryName(String tmpBeneficiaryName) {
        this.tmpBeneficiaryName = tmpBeneficiaryName;
    }

    /**
     * @return the beneficiaryEmail
     */
    public String getBeneficiaryEmail() {
        return beneficiaryEmail;
    }

    /**
     * @param beneficiaryEmail the beneficiaryEmail to set
     */
    public void setBeneficiaryEmail(String beneficiaryEmail) {
        this.beneficiaryEmail = beneficiaryEmail;
    }

    /**
     * @return the tmpBeneficiaryEmail
     */
    public String getTmpBeneficiaryEmail() {
        return tmpBeneficiaryEmail;
    }

    /**
     * @param tmpBeneficiaryEmail the tmpBeneficiaryEmail to set
     */
    public void setTmpBeneficiaryEmail(String tmpBeneficiaryEmail) {
        this.tmpBeneficiaryEmail = tmpBeneficiaryEmail;
    }

    /**
     * @return the beneficiaryPhone
     */
    public String getBeneficiaryPhone() {
        return beneficiaryPhone;
    }

    /**
     * @param beneficiaryPhone the beneficiaryPhone to set
     */
    public void setBeneficiaryPhone(String beneficiaryPhone) {
        this.beneficiaryPhone = beneficiaryPhone;
    }

    /**
     * @return the tmpBeneficiaryPhone
     */
    public String getTmpBeneficiaryPhone() {
        return tmpBeneficiaryPhone;
    }

    /**
     * @param tmpBeneficiaryPhone the tmpBeneficiaryPhone to set
     */
    public void setTmpBeneficiaryPhone(String tmpBeneficiaryPhone) {
        this.tmpBeneficiaryPhone = tmpBeneficiaryPhone;
    }

    /**
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * @return the tmpAccount
     */
    public Account getTmpAccount() {
        return tmpAccount;
    }

    /**
     * @param tmpAccount the tmpAccount to set
     */
    public void setTmpAccount(Account tmpAccount) {
        this.tmpAccount = tmpAccount;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }



    
}
