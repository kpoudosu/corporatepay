/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Embeddable
public class Location {
    @Column(name="LOC_COUNTRY")
    private String country;
    @Column(name="LOC_IP")
    private String ip;
    @Column(name="LOC_CITY")
    private String city;
    @Column(name="LOC_LATITUDE")
    private Double latitude;
    @Column(name="LOC_LONGTITUDE")
    private Double longitude;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ip).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        //return EqualsBuilder.reflectionEquals(this, obj); //fails security check,
        if (object == null) { return false; }
        if (object == this) { return true; }
        if (object instanceof Location) {
            Location rhs = (Location) object;
            return new EqualsBuilder()
                    .append(ip, rhs.ip)
                    .isEquals();
        } else {
            return false;
        }

    }
    @Override
    public String toString() {
        //return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
                append("ip", ip).
                append("country", country).
                append("city", city).
                append("latitude", latitude).
                append("longitude", longitude).
                toString();
    }
}
