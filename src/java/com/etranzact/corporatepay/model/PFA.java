/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_PFA")
@Audited
public class PFA extends StatutoryDeductionAuthority {

        @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_PFA_BANKACCOUNT", joinColumns =
    @JoinColumn(name = "PFA_ID"), inverseJoinColumns =
    @JoinColumn(name = "ACCOUNT_ID"))
    private Set<Account> accounts = new HashSet<>();
        
           @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_PFA_BANKACCOUNT", joinColumns =
    @JoinColumn(name = "PFA_ID"), inverseJoinColumns =
    @JoinColumn(name = "ACCOUNT_ID"))
    private Set<Account> tmpAccounts = new HashSet<>();

    /**
     * @return the accounts
     */
    public Set<Account> getAccounts() {
        return accounts;
    }

    /**
     * @param accounts the accounts to set
     */
    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * @return the tmpAccounts
     */
    public Set<Account> getTmpAccounts() {
        return tmpAccounts;
    }

    /**
     * @param tmpAccounts the tmpAccounts to set
     */
    public void setTmpAccounts(Set<Account> tmpAccounts) {
        this.tmpAccounts = tmpAccounts;
    }
 

  
  
        
        
    
}
