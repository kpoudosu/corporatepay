/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_SINGLE_BENEFICIARY_PAYMENT")
@Audited
public class SingleBeneficiaryPayment extends Payment{

           @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_SINGLE_BENCIAIRY_PAYMENT_BENFICIARIES", joinColumns =
    @JoinColumn(name = "ROLE_ID"), inverseJoinColumns =
    @JoinColumn(name = "MENUITEM_ID"))
    private Set<Beneficiary> beneficiaries = new HashSet<>();
                        @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();

    /**
     * @return the beneficiaries
     */
    public Set<Beneficiary> getBeneficiaries() {
        return beneficiaries;
    }

    /**
     * @param beneficiaries the beneficiaries to set
     */
    public void setBeneficiaries(Set<Beneficiary> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    
    
}
