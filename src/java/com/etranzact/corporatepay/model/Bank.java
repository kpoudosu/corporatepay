/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_ISSUER", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ISSUER_NAME", "BANK_IP"})})
@Audited
public class Bank implements Serializable {
    @Id
    @Size(min = 3,max = 6)
    @Column(name = "ISSUER_CODE")
    private String id;
    @Column(name = "ISSUER_NAME", nullable = false)
    private String bankName;
    @Column(name = "TMP_ISSUER_NAME")
    private String tmpBankName;
    @Column(name = "BANK_IP", nullable = false)
    private String bankIp;
       @Column(name = "TMP_BANK_IP")
    private String tmpBankIp;
    @Email
    @Column(name = "BANK_EMAIL")
    private String bankEmail;
      @Column(name = "TMP_BANK_EMAIL")
    private String tmpBankEmail;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
       @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
        @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIp() {
        return bankIp;
    }

    public void setBankIp(String bankIp) {
        this.bankIp = bankIp;
    }
    
    
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) { return false; }
        if (object == this) { return true; }
        if (object instanceof Bank) {
        	Bank rhs = (Bank) object;
            return new EqualsBuilder()
                    .appendSuper(super.equals(object))
                    .append(getId(), rhs.getId())
                    .isEquals();
        } else {
            return false;
        }

    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
                append("id", getId()).
                toString();
    }
    
    

    /**
     * @return the bankEmail
     */
    public String getBankEmail() {
        return bankEmail;
    }

    /**
     * @param bankEmail the bankEmail to set
     */
    public void setBankEmail(String bankEmail) {
        this.bankEmail = bankEmail;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the tmpBankName
     */
    public String getTmpBankName() {
        return tmpBankName;
    }

    /**
     * @param tmpBankName the tmpBankName to set
     */
    public void setTmpBankName(String tmpBankName) {
        this.tmpBankName = tmpBankName;
    }

    /**
     * @return the tmpBankIp
     */
    public String getTmpBankIp() {
        return tmpBankIp;
    }

    /**
     * @param tmpBankIp the tmpBankIp to set
     */
    public void setTmpBankIp(String tmpBankIp) {
        this.tmpBankIp = tmpBankIp;
    }

    /**
     * @return the tmpBankEmail
     */
    public String getTmpBankEmail() {
        return tmpBankEmail;
    }

    /**
     * @param tmpBankEmail the tmpBankEmail to set
     */
    public void setTmpBankEmail(String tmpBankEmail) {
        this.tmpBankEmail = tmpBankEmail;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    
    
}
